const WANDER_MAX_ANGLE = Math.PI * 0.05;
const WANDER_CIRCLE_DISTANCE = 3.0;
const WANDER_CIRCLE_RADIUS = 5.0;
const NEIGHBORHOOD_RADIUS = 4;

const BOID_RADIUS = 1.25;
const BOID_RADIUS_HEART = 4.0;
const BOID_RADIUS_HEAD = 2.5;
const BOID_MAX_SPEED = 60; //was 70

//MUST MATCH WITH CLIENT!
const NUM_TEAMS = 2;
const NUM_BOTS = 50;
const BOARD_SIZE = 800; // max bounds
const BOARD_BORDER = 100; // how much in from the edge is the border
const MAX_PLAYERS = 50;
const BOID_COUNT = 2000; 
const TO_WIN = 0.66;
const MAX_LIVES = 3;

const EXTRA_LIVES = 0;
const FULL_AUTO = false;
const DOMINATION = 250;
const CULL_TIME = 0.5 * 60.0;
const COOL_DOWN = 3.0;
const POINTS_PER_BASE_KILL = 1;
const POINTS_PER_LEVEL = 10;
const GROWTH_CURVE = 2.5;
//
//let mysql = require('mysql');


//
// Http
//


// const kPort = 8080;
// http.createServer((req, res) => {
//     req.on('error', (err) => {
//         console.error(err);
//     });
//     req.on('data', (data) => {

//     });
//     req.on('end', () => {
//         fileServer.serve(req, res);
//     });
// }).listen(kPort, () => {
//     console.log('Static fileserver running on port', kPort);
// });

// game time
let gameTime = 0.0;
//

//MY VECTOR2 
class Vector2 {

    constructor(x, y) {
        this.x = x || 0;
        this.y = y || 0;
    }

    dot(v) {
        return this.x * v.x + this.y * v.y;
    }

    lerp( t, v )
    {
        if (v instanceof Vector2) {
            this.x = lerp( t, this.x, v.x );
            this.y = lerp( t, this.y, v.y );
        }
        else
            throw new Error('lerp argument not vector2');
    }

    add(v) {
        if (v instanceof Vector2) {
            this.x += v.x; 
            this.y += v.y;
        }
        else
            throw new Error('add argument is not vector2');
    }

    length() {
        return Math.sqrt(this.dot(this));
    }

    distanceTo( a )
    {
        return Math.sqrt( (this.x - a.x) * (this.x - a.x) + (this.y - a.y) * (this.y - a.y) );
    }

    distanceToSqr( a )
    {
        return (this.x - a.x) * (this.x - a.x) + (this.y - a.y) * (this.y - a.y);
    }

    clone() {
        return new Vector2( this.x, this.y );
    }

    set (v){
        if (v instanceof Vector2) {
            this.x = v.x; 
            this.y = v.y;
        }
        else
            throw new Error('set argument not vector2');
    }

    subtract(v) {
        if (v instanceof Vector2) {
            this.x -= v.x; 
            this.y -= v.y;
        }
        else
            throw new Error('subtract argument not vector2');
    }

    normalize() {
        let l = this.length();
        if( l > 0 )
            this.divideScalar( this.length() );
    }

    divideScalar(v) {
        if( v !== 0 )
        {
            this.x /= v;
            this.y /= v;
        }
        else
            throw new Error('divide scalar by 0');
       
    }

    multiplyScalar(v) {
        this.x *= v;
        this.y *= v;
    }

    wrap( size ) {
        let r = false;
        //x
        if( this.x > size * 0.5 ){
            this.x -= size;
            r = true;
        } 
        else if( this.x < size * -0.5 ){ 
            this.x += size;
            r = true;
        }
        //y
        if( this.y > size * 0.5 ){
            this.y -= size;
            r = true;
        } 
        else if( this.y < size * -0.5 ){ 
            this.y += size;
            r = true;
        }

        this.x = Math.max( size * -0.5, Math.min( this.x, size * 0.5 ) );
        this.y = Math.max( size * -0.5, Math.min( this.y, size * 0.5 ) );

        //
        return r;
    }
}

class TeamHash {

    constructor() {
        this.bins = [];
        this.bins.length = NUM_TEAMS + 1;
        for( let i = 0; i < this.bins.length; i++ ) {
            this.bins[i] = [];
        }
    }

    addBoids( boids ) {
        this.clear();
        for( let i = 0; i < boids.length; i++ ){
            let boid = boids[i];
            if( boid instanceof ServerBoid )
            {
                this.bins[ boid.team ].push(boid);  
            }
        }
    }

    getBoids( team ) {
        return this.bins[ team ];
    }

    clear() {
        for( let i = 0; i < NUM_TEAMS + 1; i++ ) {
            this.bins[i].length = 0;
        }
    }

}

class FamilyHash {

    constructor() {
        this.bins = [];
        this.bins.length = MAX_PLAYERS;
        for( let i = 0; i < this.bins.length; i++ ) {
            this.bins[i] = [];
        }
    }

    addBoids( boids ) {
        this.clear();
        for( let i = 0; i < boids.length; i++ ){
            let boid = boids[i];
            if( boid instanceof ServerBoid )
            {
                if( boid.isAlive() && boid.team > 0 ) //can't be neutral and in a family, sort of a hack
                    this.bins[ boid.family ].push(boid);  
            }
        }
    }

    getBoids( family ) {
        return this.bins[family];
    }

    clear() {
        for( let i = 0; i < MAX_PLAYERS; i++ ) {
            this.bins[i].length = 0;
        }
    }

}

const GRID_SIZE = 10;
const GRID_DIM = BOARD_SIZE / GRID_SIZE;

class PositionHash {
    
    constructor() {
        this.bins = [];
        this.bins.length = GRID_DIM * GRID_DIM;
        for( let i = 0; i < this.bins.length; i++ ) {
            this.bins[i] = [];
        }
    }

    addBoids( boids ) {
        this.clear();
        for( let i = 0; i < boids.length; i++ )
        {
            let boid = boids[i];
            let binx = Math.floor( ( boid.pos.x + BOARD_SIZE * 0.5 ) / GRID_SIZE );
            let biny = Math.floor( ( boid.pos.y + BOARD_SIZE * 0.5 ) / GRID_SIZE );
            if( binx < 0 ) binx += GRID_DIM; 
            if( binx >= GRID_DIM ) binx -= GRID_DIM;
            if( biny < 0 ) biny += GRID_DIM; 
            if( biny >= GRID_DIM ) biny -= GRID_DIM;
            this.bins[ binx + GRID_DIM * biny ].push( boid );
        }
    }

    getBoids( pos, radius ) {
        let radiusSqr = radius * radius;
        let x = ( pos.x + BOARD_SIZE * 0.5 ) / GRID_SIZE;
        let y = ( pos.y + BOARD_SIZE * 0.5 ) / GRID_SIZE;
        let xMin = Math.floor( x - radius / GRID_SIZE );
        let yMin = Math.floor( y - radius / GRID_SIZE );
        let xMax = Math.ceil( x + radius / GRID_SIZE );
        let yMax = Math.ceil( y + radius / GRID_SIZE );

        let result = [];

        for( let j = yMin; j < yMax; j++ ){ 
            for( let i = xMin; i < xMax; i++ ){

                let binx = i;
                let biny = j;
                
                //skip checking bins out of bounds
                if( binx < 0 || binx >= GRID_DIM ) continue;
                if( biny < 0 || biny >= GRID_DIM ) continue;   

                let bin = this.bins[ binx + GRID_DIM * biny ];

                for( let k = 0; k < bin.length; k++ ){
                    let boidPos = bin[k].pos.clone();
                    if( pos.distanceToSqr( boidPos ) <= radiusSqr ) {
                       result.push( bin[k] );
                    }
                }
            }
        }
        return result;
    }

    clear() {
        for( let i = 0; i < GRID_DIM * GRID_DIM; i++ ) {
            this.bins[i].length = 0;
        }
    }

}

class ServerBoid {
    constructor(id) {
      
        this.pos = new Vector2( (Math.random() * 2 - 1 ) * BOARD_SIZE * 0.5, (Math.random() * 2 - 1 ) * BOARD_SIZE * 0.5);
        this.vel = new Vector2();
        this.rotation = Math.random() * 20;
        this.family = 0;
        this.team =  0;
        this.state = 0; // 0 = dead, 1 = baby, 2 = head, 3 = heart -- how to do an enum in javascript...?
        this.wanderAngle = 0;
        this.respawnTime = 0;
        this.id = id;
        this.lifetime = 0.0; // so that brand new boids can be spawned on
    }

    update(dt, boidHash, familyHash )
    {

        let myBoids = boidHash.getBoids( this.pos, NEIGHBORHOOD_RADIUS );
        let acceleration = new Vector2();

        if( !this.isAlive() )
        {
            //dead
            if( this.respawnTime <= 0 )
                this.respawn();
            else
                this.respawnTime -= dt;

            //saftey check
            this.team = 0;
        
        }
        else
        {

            this.lifetime += dt;

            if( this.isOrphan() )
            {
                this.state = 1; // prevents hanging knights

                let mult = clamp( (this.lifetime) / 5, 0, 1 );
                acceleration.add( this.wander(1.0 * mult) );
                mult *= 0.25;
                acceleration.add( this.flocking( myBoids, mult, mult, mult ) ); 
            }
            else
            {
                let player = playerBoss.getPlayer( this.family ); 
                let interact = player.interact;

                if( !player.active || !player.alive || player.team != this.team ){
                    this.team = 0;
                    return;
                }
         
                if( this.isHeart() )
                {
                    acceleration.add( this.arrive( player.pos, 1, 30.0, 1.0 ) );  
                }
                else
                {   
                    let myHeart = boids[player.heartID];

                    acceleration.add( this.wander( 0.25, true) ); 
                    acceleration.add( this.match( myHeart.pos.clone(), new Vector2( myHeart.vel.x * 0.95, myHeart.vel.y * 0.95), 20.0, 1.0 ) );
                    acceleration.add( this.flocking( myBoids, 0.3, 0.3, 0.3 ) );
                 
                    let boost = this.vel.clone();
                    boost.normalize();
                    boost.multiplyScalar( 5 );
                    acceleration.add( boost );
                }
                 
            }

            this.updateRelationship( myBoids, familyHash ); 

             
        }
         //apply velocity
        acceleration.multiplyScalar( dt );

        this.vel.add( acceleration );

        let l = this.vel.length();   

        let myMaxSpeed = BOID_MAX_SPEED;
        if( this.isHeart() ) myMaxSpeed *= 0.6;

        if ( l > myMaxSpeed )
        {
            this.vel.divideScalar( l / myMaxSpeed );
        }

        if( this.isOrphan() ){
            //add "drag" 
            let drag = this.vel.clone();
            drag.multiplyScalar( -1.0 * dt );
            this.vel.add( drag );
        }

        this.pos.x += this.vel.x * dt;
        this.pos.y += this.vel.y * dt;
            
        let playable = ( BOARD_SIZE - BOARD_BORDER ) * 0.5;
        let max = BOARD_SIZE * 0.5;
        
        //out of bounds 
    
        if(this.pos.x > playable || this.pos.x < -playable || this.pos.y > playable || this.pos.y < -playable ){
            this.lifetime = 0; // hack to make sure you don't spawn in the danger zone
            let chance = Math.random() < (dt/4);
            if( this.isBaby() && this.isAlive() && chance ) this.kill();
            if( this.isHead() && this.isAlive() && chance ) this.setBaby();
        } 

        if(this.pos.x > max || this.pos.x < -max || this.pos.y > max || this.pos.y < -max ){
            
            if( this.isHeart() ){
                let fam = familyHash.getBoids(this.family);
                //kill whole family
                for(let i = 0; i < fam.length; i++ ){
                    fam[i].kill();
                }
            }
        } 

        this.pos.x = clamp( this.pos.x, -max, max );
        this.pos.y = clamp( this.pos.y, -max, max );
    
        //rotation
        if( Math.abs( this.vel.x ) > 0.01 || Math.abs( this.vel.y ) > 0.01 ){
            this.rotation =  pilerp( dt * 8, this.rotation, -0.5 * Math.PI + Math.atan2( this.vel.y, this.vel.x ) );
        }
        
    }

    updateRelationship( myBoids, familyHash )
    {
        for( let i = 0; i < myBoids.length; i ++ ) {

            let boid = myBoids[i];

            if( !boid.isAlive() ) continue; //it's dead, no relationships with dead boids
            if( this === boid ) continue; //it's me, no relationship with self
            if( this.isSibling( boid ) ) continue; //no combat between bros
            if( boid.isOrphan() ) continue; // no combat from neutral boids 

            let distance = this.pos.distanceTo( boid.pos );       

            if( distance > this.getRadius() + boid.getRadius() ) continue;
           
            //collisons between teammates:
            if( boid.isHeart() && this.isHeart() && this.isTeammate(boid) ) { 
                    //when two hearts touch, both die
                    //this.collide( boid ); // or do nothing?
            }
            
            if( this.isTeammate(boid) ) continue; //no friendly fire

            let us = playerBoss.getPlayer( this.family );
            let them = playerBoss.getPlayer( boid.family );
            if( us.alive === 1 || them.alive === 1 ) continue; // just spawned

            // i am neutral and get tapped by an owned boid that is a heart
            if( this.isOrphan() ) // enlarged collection zone
            {   
                if( boid.isHeart() || boid.isHead() )
                {   
                    //let collector = playerBoss.getPlayer( boid.family );
                    //collector.score++;
                    this.family = boid.family;
                    this.team = boid.team;
                }
            }
            else if( this.isHeart() || boid.isHeart() )
            {
            
                if( boid.isHeart() && this.isHeart() ) { 
                    //when two hearts touch, both die
                    //this.collide( boid ); // or do nothing?
                }
                else if( this.isHeart() && !this.isTeammate( boid ) ) {  
                    // this heart was killed by another player's boid, becomes their head
                    
                    let killed = playerBoss.getPlayer( this.family );
                    let killer = playerBoss.getPlayer( boid.family );

                    if( killed.teamRank == 1 && familyHash.getBoids( this.family ).length >= 10 ) killer.lives++;
                    this.freeSiblings( familyHash.getBoids( this.family ) );
                   
                    if( killed.lives > 0 ){

                        killed.lives--;
                        killed.playtime = 0;
                        killed.alive = 1;

                    }
                    else{

                        this.setHead();
                        this.family = boid.family;
                        this.team = boid.team;

                        killer.kills ++;
                        killer.score += killed.level * killer.level * POINTS_PER_BASE_KILL;

                    }

                }
                else if( !this.isTeammate( boid ) ){
                    //boid was a heart, free siblings and claim it;
                    
                    let killed = playerBoss.getPlayer( boid.family );
                    let killer = playerBoss.getPlayer( this.family );
               
                    if( killed.teamRank == 1 && familyHash.getBoids( boid.family ).length >= 10 ) killer.lives++;
                    boid.freeSiblings( familyHash.getBoids( boid.family ) );
                    
                    if( killed.lives > 0 ){

                        killed.lives--;
                        killed.playtime = 0;
                        killed.alive = 1;

                    }
                    else{

                        boid.setHead();
                        boid.family = this.family;
                        boid.team = this.team;
                        
                        killer.kills++; 
                        killer.score += killed.level * killer.level * POINTS_PER_BASE_KILL;
                    }

                }
            }
            else if( !this.isTeammate(boid) ) 
            {
                this.collide( boid );
                if( this.isHead() ) 
                    this.setBaby();
                else 
                    this.kill();
                
                if( boid.isHead() )
                    boid.setBaby();
                else
                    boid.kill();
            }
        }
    }

    collide( boid )
    {
        //perfect elastic sphere collision
        let collision = this.pos.clone();
        collision.subtract( boid.pos );

        collision.multiplyScalar( 0.5 );
        this.pos.add( collision );
        boid.pos.subtract( collision );

        collision.normalize();
        let ac = this.vel.dot( collision );
        let bc = boid.vel.dot( collision );  
        let ai = collision.clone();
        ai.multiplyScalar( bc - ac );
        let bi = collision.clone();
        bi.multiplyScalar( ac - bc );

        this.vel.add( ai );
        boid.vel.add( bi ); 
    }

    freeSiblings( siblings )
    {
        for( let i = 0; i < siblings.length; i++ )
        {
            if( siblings[i].isHeart() ) continue;
            siblings[i].team = 0;
            siblings[i].lifetime = 0;
            siblings[i].setBaby();
        } 
    }

    convertSiblings( siblings, team )
    {
        for( let i = 0; i < siblings.length; i++ )
        {
            siblings[i].team = team;
        } 
    }

    kill()
    {
        this.state = 0;
        this.team = 0;
        this.respawnTime = 1 + Math.random() * 5;    
    }

    respawn()
    {
        let playable = (BOARD_SIZE - BOARD_BORDER) * 0.5;
        this.pos.set( new Vector2( (Math.random() * 2 - 1 ) * playable, (Math.random() * 2 - 1 ) * playable ) );
        this.vel.set( new Vector2() );
        this.rotation = Math.random() * Math.PI * 2;
        this.setBaby();
        this.family = 0; 
        this.team = 0;
        this.lifetime = 0;
    }

    orbit( target, radius, speed, amount )
    {
        let toTarget = target.clone();
        toTarget.subtract( this.pos ); 
        let dToT = toTarget.length(); 
        if( dToT > radius ) return new Vector2(); // no orbit outside of radius
        toTarget.normalize();
        //So { x, y } becomes { y | -x }.
        let tangent = new Vector2( toTarget.y, -toTarget.x );
        tangent.multiplyScalar( speed );
        if( tangent.dot( this.vel ) < 0 ) tangent.multiplyScalar( -1.0 );
        tangent.subtract( this.vel );
        tangent.multiplyScalar( amount );
        return( tangent );
    }

    seek( target, amount ){
        
        let desiredVelocity = target.clone();
        desiredVelocity.subtract( this.pos );
        if( desiredVelocity.x === 0 && desiredVelocity.y === 0 ) return new Vector2();
        desiredVelocity.normalize();
        desiredVelocity.multiplyScalar( BOID_MAX_SPEED );
        let steer = desiredVelocity.clone();
        steer.subtract( this.vel );
        steer.multiplyScalar( amount );
        return steer;
    }

    skewer( target, dir, amount )
    {
        let futureTarget = target.clone();
        let distance = this.pos.distanceTo( futureTarget );
        let offset = dir.clone();
        offset.multiplyScalar( -distance * 3/4 );
        futureTarget.add( offset );

        return this.arrive( futureTarget, 0, 5, amount );
    }

    match( pos, vel, radius, amount )
    {

        let target = pos.clone(); 
        let toTarget = pos.clone();
        toTarget.subtract( this.pos );

        let distance = toTarget.length();

        let offset = vel.clone();
        offset.normalize();
        offset.multiplyScalar( -distance * 0.5 );
        target.add( offset );

        let desiredVelocity = target.clone();
        desiredVelocity.subtract( this.pos );
        desiredVelocity.normalize();
        desiredVelocity.multiplyScalar( BOID_MAX_SPEED );
        
        let blend = clamp( distance / radius, 0, 1 ); // goes to 1 as you reach edge of radius
        vel.lerp( blend, desiredVelocity );
        
        let steer = vel.clone();
        steer.subtract( this.vel );
        steer.multiplyScalar( amount );

        return steer;

    }

    arrive( target, targetRadius, slowingRadius, amount ){
        
        let desiredVelocity = target.clone();
        desiredVelocity.subtract( this.pos );
        let distance = desiredVelocity.length();
        
        if (distance < slowingRadius + targetRadius ) {
            desiredVelocity.normalize();
            let mult = BOID_MAX_SPEED * Math.max( 0, distance - targetRadius ) / ( slowingRadius );
            desiredVelocity.multiplyScalar( mult );   
        } 
        else {
            desiredVelocity.normalize();
            desiredVelocity.multiplyScalar( BOID_MAX_SPEED );
        }
        
        let steer = desiredVelocity.clone();
        steer.subtract( this.vel );
        steer.multiplyScalar( amount );

        return steer;
    }

    flocking( boids, separation, cohesion, alignment ) {
       
        //separation
        let boid, distance;
        let separationSum = new Vector2();
        //cohesion
        let cohesionSum = new Vector2();
        let cohesionCount = 0;
        //alignment
        let alignmentSum = new Vector2();
        let alignmentCount = 0;

        let acceleration = new Vector2();
        
        for ( let i = 0; i < boids.length; i ++ ) {

            boid = boids[ i ];
            if( !boid.isAlive() ) continue; // no flocking for dead boids
            if( this === boid ) continue;
            
            let distance = this.pos.distanceTo( boid.pos );       
          
            if( this.isTeammate( boid ) )
            {   
                //separation
                let repulse = new Vector2();
                repulse.add( this.pos );
                repulse.subtract( boid.pos );
                repulse.normalize();
                repulse.multiplyScalar( BOID_MAX_SPEED );
                let realD = Math.max( distance - this.getRadius() - boid.getRadius(), 0 ); 
                if( realD > 1 ) repulse.divideScalar( realD );
                separationSum.add( repulse );
                //cohesion
                cohesionCount++;
                cohesionSum.add( boid.pos );
                //alignment
                alignmentCount++;
                alignmentSum.add( boid.vel );
            }       
        }

        separationSum.multiplyScalar( separation );
        acceleration.add( separationSum );

        if ( cohesionCount > 0 ) {
            cohesionSum.divideScalar( cohesionCount );
            cohesionSum.subtract( this.pos );
            cohesionSum.multiplyScalar( cohesion );
            acceleration.add( cohesionSum );

        if( alignmentCount > 0 )
            alignmentSum.divideScalar( alignmentCount );
            alignmentSum.normalize();
            alignmentSum.multiplyScalar( this.vel.length() );
            alignmentSum.subtract( this.vel );
            alignmentSum.multiplyScalar( alignment );
            acceleration.add( alignmentSum );
        }

        return acceleration;
    }

    wander( amount, preserveVel = false ) {

        let circleCenter = this.vel.clone();
        circleCenter.normalize();
        circleCenter.multiplyScalar( WANDER_CIRCLE_DISTANCE ); //CIRCLE DISTANCE
        this.wanderAngle += WANDER_MAX_ANGLE * ( Math.random() * 2 - 1 );
        let displacement = new Vector2( Math.sin( this.wanderAngle ), Math.cos( this.wanderAngle ) );
        displacement.multiplyScalar( WANDER_CIRCLE_RADIUS );
        let steer = new Vector2();
        steer.add( circleCenter );
        steer.add( displacement );
        if( preserveVel )
        {
            steer.normalize();
            steer.multiplyScalar( this.vel.length() );
        }
        steer.subtract( this.vel );
        steer.multiplyScalar( amount );
        return steer;
    }

    serialize(cameraPos, cameraDiag, dataView, offset) {
        
        /*
        10 bytes per boid
        t0) 16: id(12) team(4)
        t1) 32: family (8) state (2) posx (11) posy (11)
        t2) 32: rotation (10) velx (11) vely (11) 
        */

        let t0 = 0;
        t0 = t0 | ( this.id << 4 );  // 12
        t0 = t0 | ( this.team ); // 4

        let t1 = 0;
        t1 = t1 | ( this.family << 24 ); //8
        t1 = t1 | ( this.state << 22 ); //2
        t1 = t1 | ( compact( this.pos.x - cameraPos.x, -cameraDiag, cameraDiag, 0x7FF ) << 11 ); //11
        t1 = t1 | ( compact( this.pos.y - cameraPos.y, -cameraDiag, cameraDiag, 0x7FF ) ); //11

        let velRange = 100.0;

        let t2 = 0;
        t2 = t2 | ( compact( this.rotation, -Math.PI, Math.PI, 0x3FF ) << 22 ); //10
        t2 = t2 | ( compact( this.vel.x, -velRange/2, velRange/2, 0x7FF ) << 11 ); //11
        t2 = t2 | ( compact( this.vel.y, -velRange/2, velRange/2, 0x7FF ) ); //11

        dataView.setUint16( offset, t0 );
        offset += 2;
        dataView.setUint32( offset, t1 );
        offset += 4;
        dataView.setUint32( offset, t2 );
        offset += 4;

        return offset;
        
    }

    //some helpers
    getRadius() {
        if( this.isHeart() ) return BOID_RADIUS_HEART;
        else if( this.isHead() ) return BOID_RADIUS_HEAD;
        else return BOID_RADIUS;
    }

    isRelated( boid ){ return (this.family + boid.family) % 2 === 0; } //both are even or both are odd
    isSibling( boid ){ return this.family === boid.family && this.isTeammate(boid); }
    isTeammate( boid ){ return this.team === boid.team; }
    isOrphan() { return this.team === 0; }
    isAlive() { return this.state > 0; }
    isHeart() { return this.state === 3; }
    isHead() { return this.state === 2; }
    isBaby() { return this.state === 1; }
    setBaby() { this.state = 1; }
    setHead() { this.state = 2; }
    setHeart() { this.state = 3; }
        
}

let boids = [];
for( let i = 0; i < BOID_COUNT; i++ )
{
    boids[i] = new ServerBoid(i);
}

let boidHash = new PositionHash();
let familyHash = new FamilyHash();
let teamHash = new TeamHash();

let bytesIn = 0;
let bytesOut = 0;

let names = ["Asher","Atticus","Auden","August","Axl","Beckett","Brax","Bruno","Byron","Dashiell","Dexter","Django","Duke","Edison","Elvis","Everitt","Fenton","Fitz","Frances","Greer","Gulliver","Gus","Holden","Homer","Hugo","Ignatius","Ike","Jagger","Jasper","Kingston","Leopold","Levi","Lionel","Luca","Magnus","Miles","Milo","Monty","Moses","Nico","Nix","Odin","Orlando","Orson","Oscar","Otis","Otto","Poppy","Prince","Ray","Roman","Roscoe","Rufus","Salinger","Sebastian","Stella","Sullivan","Tennyson","Theo","Waldo","Zane","Zeus","Zoe"];

class GameData {

    constructor() {
        this.states = [];
        this.teams = [];
        this.pops = [];
        this.scores = [];
        this.lives = [];
        this.names = [];
        //this.posYs = [];
        //this.posXs = []
        this.ranks = [];
        this.levels = [];
        this.alives = [];
        this.unloaded = true;
    }

    load(){

        this.unloaded = false;
        for(let i = 0; i < playerBoss.players.length; i++ ){

            this.states[i] = playerBoss.players[i].active ? ( playerBoss.players[i].bot ? 2 : 1 ) : 0;
            this.teams[i] = playerBoss.players[i].team;
            this.pops[i] = playerBoss.players[i].pop;
            this.scores[i] = playerBoss.players[i].score;
            this.lives[i] = playerBoss.players[i].lives;
            this.names[i] = playerBoss.players[i].name;
            this.ranks[i] = playerBoss.players[i].teamRank;
            this.levels[i] = playerBoss.players[i].level;
            this.alives[i] = playerBoss.players[i].alive;

        }
    }    

}

class GameDataPackage {

    constructor(){
        this.states = [];
        this.teams = [];
        this.pops = [];
        this.scores = [];
        this.lives = [];
        this.names = [];
        this.ranks = [];
        this.levels = [];
        this.alives = [];
        
    }

    diff( gameDataOld, gameDataNew ){ //can this still work if gameDataOld is null

        //go through names and push any names that are out of date into my names array
        for( let i = 0; i < playerBoss.players.length; i++ ){

            if( gameDataOld.unloaded || gameDataOld.states[i] !== gameDataNew.states[i] ){
                var d = [ gameDataNew.states[i], i ];
                this.states.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.teams[i] !== gameDataNew.teams[i] ){
                var d = [ gameDataNew.teams[i], i ];
                this.teams.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.pops[i] !== gameDataNew.pops[i] ){
                var d = [ gameDataNew.pops[i], i ];
                this.pops.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.scores[i] !== gameDataNew.scores[i] ){
                var d = [ gameDataNew.scores[i], i ];
                this.scores.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.lives[i] !== gameDataNew.lives[i] ){
                var d = [ gameDataNew.lives[i], i ];
                this.lives.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.names[i] !== gameDataNew.names[i] ){
                var d = [ gameDataNew.names[i], i ];
                this.names.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.ranks[i] !== gameDataNew.ranks[i] ){
                var d = [ gameDataNew.ranks[i], i ];
                this.ranks.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.levels[i] !== gameDataNew.levels[i] ){
                var d = [ gameDataNew.levels[i], i ];
                this.levels.push( d );
            }

            if( gameDataOld.unloaded || gameDataOld.alives[i] !== gameDataNew.alives[i] ){
                var d = [ gameDataNew.alives[i], i ];
                this.alives.push( d );
            }

        }



    }

    serialize( dataView, offset ){

        //states
        //teams
        //pops
        //scores
        //lives
        //names
        //rank
        //levels
        //alive

         // states
        dataView.setUint8(offset, this.states.length );
        offset += 1;
        for( let i = 0; i < this.states.length; i++ ){

            let id = this.states[i][1];
            let state = this.states[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint8(offset, state ); //state
            offset +=1; 
        }

        dataView.setUint8(offset, this.teams.length );
        offset += 1;
        for( let i = 0; i < this.teams.length; i++ ){

            let id = this.teams[i][1];
            let team = this.teams[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint8(offset, team ); //state
            offset +=1; 
        }

        // pops
        dataView.setUint8(offset, this.pops.length );
        offset += 1;
        for( let i = 0; i < this.pops.length; i++ ){

            let id = this.pops[i][1];
            let pop = this.pops[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint16(offset, pop ); //pop
            offset +=2; 
        }

         // scores
        dataView.setUint8(offset, this.scores.length );
        offset += 1;
        for( let i = 0; i < this.scores.length; i++ ){

            let id = this.scores[i][1];
            let score = this.scores[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint16(offset, score ); //score
            offset +=2; 
        }

         // lives
        dataView.setUint8(offset, this.lives.length );
        offset += 1;
        for( let i = 0; i < this.lives.length; i++ ){

            let id = this.lives[i][1];
            let lives = this.lives[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint8(offset, lives ); //lives
            offset +=1; 
        }

        // names
        dataView.setUint8(offset, this.names.length );
        offset += 1;
        for( let i = 0; i < this.names.length; i++ ){

            let id = this.names[i][1];
            let name = this.names[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;

            let nameLength = name.length;

            if( nameLength > 15 ) nameLength = 15; 
            dataView.setUint8(offset, nameLength ); //length of name
            offset += 1;

            for (let x = 0; x < nameLength; x++)
            {
                dataView.setUint8(offset, name.charCodeAt(x) );
                offset += 1;
            }   
        }

        // ranks
        dataView.setUint8(offset, this.ranks.length );
        offset += 1;
        for( let i = 0; i < this.ranks.length; i++ ){

            let id = this.ranks[i][1];
            let rank = this.ranks[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint8(offset, rank ); //rank
            offset +=1; 
        }

         // levels
        dataView.setUint8(offset, this.levels.length );
        offset += 1;
        for( let i = 0; i < this.levels.length; i++ ){

            let id = this.levels[i][1];
            let level = this.levels[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint8(offset, level ); //rank
            offset +=1; 
        }

        //alive
        dataView.setUint8(offset, this.alives.length );
        offset += 1;
        for( let i = 0; i < this.alives.length; i++ ){

            let id = this.alives[i][1];
            let alive = this.alives[i][0];

            dataView.setUint8(offset, id); // id
            offset +=1;
            dataView.setUint8(offset, alive ); //rank
            offset +=1; 
        }

        return offset;

    }

    getBytesNeeded(){

        //8 bytes min per player
        //could be bit packed as bools
        let needed = 0;
        needed += 1 + this.names.length * (15+1+1);//name length and id
        needed += 1 + this.pops.length * (3);
        needed += 1 + this.lives.length * (2);
        needed += 1 + this.scores.length * (3);
        needed += 1 + this.states.length * (2);
        needed += 1 + this.teams.length * (2);
        needed += 1 + this.ranks.length * (2);
        needed += 1 + this.levels.length * (2);
        needed += 1 + this.alives.length * (2);

        return needed;
    }


}
let sync = 0;

class Player {

    constructor() {
        this.socket = null;
        this.id = 0;
        this.pos = new Vector2();
        this.interact = false;
        this.boss = null;
        this.active = false;
        this.heartID = 0;
        this.respawnTime = 1.0;
        this.kills = 0;
        this.pop = 0;
        this.bot = false;
        this.score = 0;
        this.level = 1;
        this.lives = 0;
        this.teamRank = 0;
        this.timeSinceSend = 0;

        this.spawns = 0;
        this.pointsPending = 0;

        this.alive = 0; 
        this.playtime;

        this.botOffset = new Vector2( Math.random() * 20.0 - 10.0, Math.random() * 20 - 10);
        let first = names[ Math.floor( Math.random() * names.length ) ];
        let last = "Bot";

        this.camera = new Vector2(0.0,0.0);
        this.cameraDiag = 0.0;

        this.name = first+" "+last;

        this.team = 0;
        this.clientState = 0;

        this.lastAckd = 0;
        this.sequenceNumber = 0;
        this.gameDataBuffer = [];

        for( var i = 0; i < 30; i++ ){
            this.gameDataBuffer[i] = new GameData();
        }
    }

    setSocket(socket) {
        this.socket = socket;

        //socket.setNoDelay();

        socket.binaryType = "arraybuffer";
        socket.on('error', (error) => {
            console.log(error);
            this.boss.free(this);
        });
        socket.on('close', (code, message) => {
            console.log("closed: (%d): %s", code, message);
            this.socket = null;
            this.boss.free(this);
        });
        socket.on('message', (data, flags) => {
            // Process the player's message
            {
         
                bytesIn += data.length;
                
                let arrayBuffer = null;
                if (typeof(data) === "object") {
                    if (data instanceof ArrayBuffer) { arrayBuffer = data; }
                    if (data instanceof Buffer) {
                        arrayBuffer = new ArrayBuffer(data.length);
                        let view = new Uint8Array(arrayBuffer);
                        for (let i = 0; i < data.length; i++) {
                            view[i] = data[i];
                        }
                    }
                }

                if (arrayBuffer && arrayBuffer.byteLength > 0) {

                    let dataView = new DataView(arrayBuffer);
                    let offset = 0;
                    this.cameraDiag = decompress16( dataView, offset, BOARD_SIZE );
                    this.cameraDiag *= 1.1;// extra 10% buffer
                    offset += 2;

                    this.pos.x = decompress16( dataView, offset, BOARD_SIZE );
                    this.pos.x -= BOARD_SIZE / 2;
                    offset += 2;
                    this.pos.y = decompress16( dataView, offset, BOARD_SIZE );
                    this.pos.y -= BOARD_SIZE / 2;
                    offset += 2;
                    //
                    this.interact = dataView.getUint8(offset) === 1;
                    offset += 1;

                    this.clientState = dataView.getUint8(offset);
                    offset += 1;
                    //
                    this.lastAckd = dataView.getUint16(offset);
                    offset += 2;

                    let recName = dataView.getUint8(offset);
                    offset += 1;

                    if( recName ){  
                        let nameLength = dataView.getUint8(offset);
                        offset += 1;
                        if( nameLength > 15) nameLength = 15;
                        let name = "";
                        for (let x = 0; x < nameLength; x++){
                            name += String.fromCharCode( dataView.getUint8(offset) );
                            offset += 1;
                        }
                        this.name = name;
                    }

                }
            }

           

            // let localBoids = boidHash.getBoids( this.camera, this.cameraDiag );
            // let localBoidCount = localBoids.length;

            // //create game state object
            // //and set in in the buffer
            // //and increase the sequence number
            // this.sequenceNumber++;
            // if( this.sequenceNumber > this.gameDataBuffer.length - 1 ){ this.sequenceNumber = 0; }

            // this.gameDataBuffer[ this.sequenceNumber ].load();

            // let gameDataPackage = new GameDataPackage();

            // gameDataPackage.diff( this.gameDataBuffer[ this.lastAckd ], this.gameDataBuffer[this.sequenceNumber] );

            // let boidBytes = 10 * localBoidCount + 2 + 6; //2 for local count total, 6 for camera pos and diag

            // let gameStateSize = 3 + 2 + 1 + gameDataPackage.getBytesNeeded(); // plus 1 for local player id

            // let arrayBuffer = new ArrayBuffer(  gameStateSize + boidBytes ); //+ playerSize 
            // let dataView = new DataView(arrayBuffer);
            // let offset = 0;

            // //local player id
            // dataView.setUint8(offset, this.id ); 
            // offset += 1;
            // //game state
            // dataView.setUint8(offset, gameState );
            // offset += 1;
            // compress16( dataView, offset, gameTime, 2000.0 ); //+2 bytes
            // offset += 2;
            // //
            // dataView.setUint16(offset, this.sequenceNumber );
            // offset += 2;

            // offset = gameDataPackage.serialize( dataView, offset );

            // //16 bits compact( this.pos.x, -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF )
            // dataView.setUint16(offset, compact( this.camera.x, -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF ) );
            // offset += 2;
            // dataView.setUint16(offset, compact( this.camera.y, -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF ) );
            // offset += 2;
            // dataView.setUint16(offset, compact( this.cameraDiag, 0, BOARD_SIZE * 2, 0xFFFF ) );
            // offset += 2;

            // dataView.setUint16(offset, localBoidCount );
            // offset += 2;

            // for( let i = 0; i < localBoidCount; i++ )
            // {   
            //     offset = localBoids[i].serialize(this.camera, this.cameraDiag, dataView, offset);
            //     if( localBoids[i].isHeart() ){
            //         console.log( localBoids[i].pos.x + "," + localBoids[i].pos.y );
            //     }
            // }

            // bytesOut += arrayBuffer.byteLength;

            // socket.send(arrayBuffer);
              
      
        });
        socket.on('ping', (data, flags) => {
            //@TODO: Figure out what to do with these guys ... -JCE 1-17-2017
            //console.log('ping: %s', data);
        });
        socket.on('pong', (data, flags) => {
            //console.log('pong: %s', data);
        });
    }

    update( dt ){

        //camera logic
        if( !this.bot && this.spawns > 0 && boids[this.heartID].team != 0 ){
             this.camera.set( boids[this.heartID].pos );
        }
        else{
            this.camera.x = this.camera.y = 0.0;
        }

        if( this.active && this.clientState == 0 ){
            //at the title screen, reset everything
            this.lives = EXTRA_LIVES;
            this.score = 0;
            this.kills = 0;
            this.pop = 0;
            this.alive = 0;
            this.level = 1;
            this.respawntime = 5.0;
            this.spawns = 0; //right place to do this?
        }
        else if( this.active && this.clientState == 2 )
        {
            if( this.alive > 0 ) this.playtime += dt;

            if( this.score >= Math.floor( Math.pow( this.level, GROWTH_CURVE ) * POINTS_PER_LEVEL ) ){ 
                this.level++;
                this.lives++;
            }

            this.lives = clamp( this.lives, 0, MAX_LIVES );

            if( this.alive === 1 && this.playtime > COOL_DOWN ){
                this.alive = 2;
            }

            if( this.bot && this.alive > 0 )
            {
               // BOT "AI"
                this.pos.x =  boids[this.heartID].pos.x + this.botOffset.x;
                this.pos.y = boids[this.heartID].pos.y + this.botOffset.y;

                let force = false;
              
                let playable = (BOARD_SIZE - BOARD_BORDER) * 0.5;
                if( Math.abs( this.pos.x ) > playable || Math.abs( this.pos.y ) > playable ) force = true;
                this.pos.x = clamp( this.pos.x, -playable, playable );
                this.pos.y = clamp( this.pos.y, -playable, playable );
            
                if( Math.random() < dt / 4 || force )
                {
                    let angle = Math.random() * Math.PI * 2;
                    let magnitude = 5.0 + Math.random() * 25.0;

                    this.botOffset.x = Math.sin( angle ) * magnitude;
                    this.botOffset.y = Math.cos( angle ) * magnitude;

                }
                
            }

            if( !boids[this.heartID].isHeart() && this.alive > 0 ) //heart is no longer a king
            {
                this.alive = 0;
                this.playtime = 0;
                //just killed
                this.respawnTime = 5.0;
            
                //wait to reset real players so final score/levels can be reported 
                if( this.bot ){
                    this.level = 1;
                    this.kills = 0;
                    this.score = 0;
                    this.spawns = 0;
                    this.lives = EXTRA_LIVES;
                }
                else{
                     MySQLPushHighScore( this.name, this.level, this.score, this.playtime, false );
                }

                //temp
                this.kills = 0;
                this.scores = 0;
                
            }

            //respawn logic
            this.respawnTime -= dt;
            
            if( this.alive === 0 && this.respawnTime < 0 )
            {
                if( this.spawns === 0 ){
                    this.team = GetWeakerTeam( this.bot );
                }

                this.spawns++;

                let boidsRanked = boids.slice();
                boidsRanked.sort( boidLifetimeSort ); // chose oldest free boids

                for( let i = 0; i < boidsRanked.length; i++ )
                {
                    let boid = boidsRanked[i];
                    if( boid.isOrphan() && boid.isBaby() )
                    {
                        this.heartID = boid.id;
                        boid.setHeart();
                        boid.family = this.id; 
                        boid.team = this.team;
                        this.alive = 1;
                        return;
                    }
                }

                //if no free orphans, then canabalize
        
                for( let i = 0; i < boidsRanked.length; i++ )
                {
                    let boid = boidsRanked[i];
                    if( boid.team == this.team && boid.isBaby() )
                    {
                        this.heartID = boid.id;
                        boid.setHeart();
                        boid.family = this.id; 
                        boid.team = this.team;
                        this.alive = 1;
                        return;
                    }
                }

            }
        }

        this.timeSinceSend += dt;

        if( this.socket !== null && this.timeSinceSend > 1.0/10.0 ){

            let localBoids = boidHash.getBoids( this.camera, this.cameraDiag );
            let localBoidCount = localBoids.length;

            //create game state object
            //and set in in the buffer
            //and increase the sequence number
            this.sequenceNumber++;
            if( this.sequenceNumber > this.gameDataBuffer.length - 1 ){ this.sequenceNumber = 0; }

            this.gameDataBuffer[ this.sequenceNumber ].load();

            let gameDataPackage = new GameDataPackage();

            gameDataPackage.diff( this.gameDataBuffer[ this.lastAckd ], this.gameDataBuffer[this.sequenceNumber] );

            let boidBytes = 10 * localBoidCount + 2 + 6; //2 for local count total, 6 for camera pos and diag

            let gameStateSize = 1 + 2 + 2 + gameDataPackage.getBytesNeeded(); // plus 1 for local player id

            let arrayBuffer = new ArrayBuffer(  gameStateSize + boidBytes ); //+ playerSize 
            let dataView = new DataView(arrayBuffer);
            let offset = 0;

            //local player id
            dataView.setUint8(offset, this.id ); 
            offset += 1;

            compress16( dataView, offset, gameTime, 2000.0 ); //+2 bytes
            offset += 2;
            //
            dataView.setUint16(offset, this.sequenceNumber );
            offset += 2;

            offset = gameDataPackage.serialize( dataView, offset );

            //16 bits compact( this.pos.x, -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF )
            dataView.setUint16(offset, compact( this.camera.x, -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF ) );
            offset += 2;
            dataView.setUint16(offset, compact( this.camera.y, -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF ) );
            offset += 2;
            dataView.setUint16(offset, compact( this.cameraDiag, 0, BOARD_SIZE * 2, 0xFFFF ) );
            offset += 2;

            dataView.setUint16(offset, localBoidCount );
            offset += 2;

            for( let i = 0; i < localBoidCount; i++ )
            {   
                offset = localBoids[i].serialize(this.camera, this.cameraDiag, dataView, offset);
            }

            bytesOut += arrayBuffer.byteLength;

            this.socket.send(arrayBuffer);

            this.timeSinceSend = 0.0;
        }

    }

}

class PlayerBoss {
    constructor() {
        this.players = [];
        this.players.length = MAX_PLAYERS;
        for (let i = 0; i < this.players.length; i++) {
            this.players[i] = new Player();
            this.players[i].id = i;
        }
    }

    alloc() {
        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            if (!p.active) {
                p.boss = this;
                p.active = true;
                p.lives = EXTRA_LIVES;
                p.team = 1 + (i%2);
                p.lastAckd = 0;
                p.sequenceNumber = 0;
                p.gameDataBuffer[0].unloaded = true; // very important, otherwise fresh data never gets sent!
                p.playtime = 0;
                p.alive = 0;
                return p;
            }
        }
        //take a bot if not enough room
        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            if( p.bot )
            {
                console.log("freeing bot to make space");
                p.boss.free(p);
                return playerBoss.alloc();   
            }
        }
        return null;
    }


    getPlayer( x ) {
        x = clamp( x, 0, this.players.length );
        return this.players[ x ];
    }

    free(player) {
        player.boss = null;

        if( boids[player.heartID].isHeart()) //heart is still a king
        {
            //hack to make removing bots work...?
            boids[player.heartID].kill();
        }

        if( !player.bot ){
            MySQLPushHighScore( player.name, player.level, player.score, player.playtime, true);
        }

        player.alive = 0;
        player.bot = false;
        player.active = false;
        player.clientState = 0;
        player.kills = 0;
        player.score = 0;
        player.pop = 0;
        player.lives = 0;
        player.level = 1;
        player.spawns = 0;

    }

    getActivePlayerCount() {
        let activeCount = 0;
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].active) {
                ++activeCount;
            }
        }
        return activeCount;
    }

    getBotCount() {
        let botCount = 0;
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].bot && this.players[i].active ) {
                ++botCount;
            }
        }
        return botCount;
    }

    getRealCount() {
        let realCount = 0;
        for (let i = 0; i < this.players.length; i++) {
            if (!this.players[i].bot && this.players[i].active ) {
                ++realCount;
            }
        }
        return realCount;
    }

}

///so weird

let http = require('http');
const express = require('express');
const url = require('url');
const WebSocket = require('ws');
require('console-stamp')(console, {pattern: 'yyyy-mm-dd HH:MM:ss.L'});

let io_app = express();
let dir = __dirname + '/../public';
io_app.use(express.static(dir));

let server = http.createServer(io_app);
let ws = new WebSocket.Server({ server });

let playerBoss = new PlayerBoss();

ws.on('connection', function(ws, request) {

    console.log( "new ws connection" );
    console.log( ws );
    console.log( request );

    let loc = url.parse(request.url, true).path;

    ws.name = request.connection.remoteAddress + ':' + request.connection.remotePort;
    //console.log('Connected %s to %s', ws.name, loc);

    if (loc == '/play') {

        let player = playerBoss.alloc();
        if (player) {
            player.setSocket(ws);

        } else {
            socket.close();
        }
    }

});

const kPort = process.env.NODE_ENV == 'production' ? 80 : 8080;
server.listen(kPort, function() {
    console.log('Server listening on port %d', server.address().port);
});

//




//
// Update loop
//
let timestampOld = 0.0;
let timeStampDelay = 1.0;
let timeStampCount = 0;
let timeStampAvg = 0;

let timeSinceRanking = 0.0;

let slowChannelDelay = 0;

function update() {

    // Get dt
    let hrtime = process.hrtime();
    let timestamp = hrtime[0] + 1.0e-9 * hrtime[1];
    let dt = Math.min(0.1, timestamp - timestampOld);

    timestampOld = timestamp;

    slowChannelDelay += dt;

    //my awesome profliling acheivement...
    timeStampAvg += dt;
    timeStampCount ++;
    timeStampDelay -= dt;
    if( timeStampDelay <= 0 )
    {
        let util = require('util');
        let memoryUsage = util.inspect(process.memoryUsage());
        timeStampDelay = 5.0;
        timeStampAvg /= timeStampCount;
        bytesOut /= timeStampDelay;
        bytesIn /= timeStampDelay;

        // rounding
        let kbsOut = Math.round(bytesOut / 1000 * 100) / 100;
        let kbsIn = Math.round(bytesIn / 1000 * 100) / 100;

        console.log( "-------------------------");
        console.log( "Status - Average FPS:"+1.0 / timeStampAvg+" "+playerBoss.getActivePlayerCount()+" players" );
        //console.log( "Memory - "+memoryUsage );
        console.log( "Network - In:"+kbsIn+"kbs | Out:"+kbsOut+"kbs");
        console.log( "Time - "+gameTime ); 
        
        timeStampAvg = 0;
        timeStampCount = 0;
        bytesOut = 0;
        bytesIn = 0;
    }

    if( playerBoss.getRealCount() == 0 ) return; //no need to update if no players connected (something about a tree falling?)

    teamHash.addBoids( boids );

  
    let teamACount = teamHash.getBoids(1).length;
    let teamBCount = teamHash.getBoids(2).length;

    let suddenDeath = Math.abs( teamACount - teamBCount ) > DOMINATION  && playerBoss.getActivePlayerCount() > 2;

    if( suddenDeath && gameTime > 10.0 ){
        gameTime -= dt * 2; // clock ticks down faster
    }
    else{
         gameTime -= dt;
    }

    if( gameTime <= 0.0 ){

        gameTime = CULL_TIME;
    }

    if( suddenDeath ){
         //rebalance
        console.log("Culling");
        console.log( "A:"+teamACount );
        console.log( "B:"+teamBCount );

        //endgame(); kills losers
        rebalance();
    }


    //
    {

        boidHash.addBoids( boids );   
        familyHash.addBoids( boids ); 
        
        for( let i = 0; i < boids.length; i++ ){
            boids[i].update(dt, boidHash, familyHash );    
        }

        //update player, ranks and pop
        for( let i = 0; i < playerBoss.players.length; i++ ){
            let player = playerBoss.players[i];
            player.pop = familyHash.getBoids( player.id ).length;  
            player.update(dt); 
        }

        if( timeSinceRanking > 1.0 ){

            let playersRanked = playerBoss.players.slice();
            playersRanked.sort( playerDataSortFunction );
            let aRank = 1;
            let bRank = 1;

            for( let i = 0; i < playersRanked.length; i++ ){

                let player = playersRanked[i];
                if( player.team == 1){
                    player.teamRank = aRank;
                    aRank++;
                }
                else if(player.team == 2){
                    player.teamRank = bRank;
                    bRank++;
                }
                else{
                    player.teamRank = 0;
                }
            }

            timeSinceRanking = 0.0;

        }

        timeSinceRanking += dt;

    }

    let bots = playerBoss.getBotCount();
    //bots can't be removed, other than when a real player joins (for now?)
    if( bots < NUM_BOTS && playerBoss.getActivePlayerCount() < MAX_PLAYERS ){ 
        let player = playerBoss.alloc();
        player.bot = true;
        player.clientState = 2;
    }

}

function endgame(){
        //respawn al free boids
    // let freeBoids = teamHash.getBoids(0);
    // for( let i = 0; i < freeBoids.length; i++ ){
    //     //freeBoids[i].kill();
    // }  

    let teamACount = teamHash.getBoids(1).length;
    let teamBCount = teamHash.getBoids(2).length;

    for( let i = 0; i < playerBoss.players.length; i++ ){

        let player = playerBoss.players[i];   
        if( (player.team == 2 && teamBCount < teamACount) || (player.team == 1 && teamACount < teamBCount ) ){
            //lost
            if( player.bot ) return; // bots don't die from losing rounds, keeps more boids in play

            if( player.lives > 1){ 
               player.lives--;
            }
            else{
                // on your last life, kill em all
                let fam = familyHash.getBoids(player.id);
                for( let j = 0; j < fam.length; j++ ){
                    fam[j].kill();
                }  
            }
        }
        else{
            //winner
            player.level++;
        }
    }


}

function rebalance(){

    console.log("rebalancing teams");

    // where should players be placed when they start? red side and blue side? Random, a grid? 
    // Maybe around a cicle. Spread out for however many players there are?

    //kill half each players boids
    //reset positions
    let activePlayers = playerBoss.getActivePlayerCount();

    //just to be safe...
    teamHash.addBoids( boids ); 
    familyHash.addBoids( boids );  

    let teamCounts = [ 0, teamHash.getBoids(1).length, teamHash.getBoids(2).length ]
    let mc = BOID_COUNT /4;
    let teamGoals = [ 0, Math.min( Math.min( teamCounts[1], teamCounts[2] ), mc ), Math.min( Math.min( teamCounts[1], teamCounts[2] ), mc ) ];

    //pruning
    for( let i = 0; i < playerBoss.players.length; i++ ){

        let player = playerBoss.players[i];
        if( !player.active || !player.alive ) continue;

        let lossRatio = 1.0 - teamGoals[ player.team ] / teamCounts[ player.team ]; 
        //lossRatio *= 0.5;// this should be tuned 

        console.log( "Player " + player.id + " before pruning " + player.pop + " loss ratio " + lossRatio );
        console.log( teamGoals[ player.team ] +" "+ teamCounts[ player.team ] );

        let fam = familyHash.getBoids(player.id);
        let toKill = Math.floor( fam.length * lossRatio ); 
        console.log( "to kill " + toKill );
        for( let j = 0; j < fam.length; j++ ){
                
                if( toKill <= 0 ) break;
                if( fam[j].isHeart() ) continue;
                
                fam[j].kill();
                toKill--;
                teamCounts[player.team]--;
                player.pop--;
                player.pointsPending++;
                
        } 
    }

    //rich give up first
    
    let attempts = Math.abs( teamCounts[1] - teamCounts[2] ); // should < number of boids taht need to be removed no?

    while( teamCounts[1] != teamCounts[2] && attempts > 0 ){

        familyHash.addBoids( boids );

        let playersRanked = playerBoss.players.slice();
        playersRanked.sort( playerDataSortFunction );

        let ahead = teamCounts[1] > teamCounts[2] ? 1 : 2;

        let cleared = false;
        for( let i = 0; i < playersRanked.length; i++ )
        {
            if( cleared ) break;
            let player = playersRanked[i];
            if( player.team == ahead ){

                let fam = familyHash.getBoids(player.id);
                for( let j = 0; j < fam.length; j++ ){
                
                    if( cleared ) break;
                    if( fam[j].isHeart() ) continue;
                    
                    fam[j].kill();
                    teamCounts[player.team]--;
                    player.pop--;
                    player.pointsPending++;
                    cleared = true;
                    
                }      
            }
        }

        //nothing to kill
        attempts--;
    }

    //add bonuses!
    for( let i = 0; i < playerBoss.players.length; i++ ){
        let player = playerBoss.players[i];
        if( player.active ){
            player.score += Math.ceil(player.pointsPending / 10); //maybe times level?
        }
        player.pointsPending = 0;
    }

    console.log( "Teams rebalanced " + teamCounts[1] + " vs " + teamCounts[2] );

    teamHash.addBoids( boids );

    // let freeBoids = teamHash.getBoids(0);
    // for( let i = 0; i < freeBoids.length; i++ ){
    //     freeBoids[i].respawn();
    //     freeBoids[i].lifetime = 10.0;
    // }  
  
}

setInterval(update, 10); //60ish fps


function GetWeakerTeam( bot ){

    let teamAPlayers = 0;
    let teamBPlayers = 0;
    let teamAHumans = 0;
    let teamBHumans = 0;

    for( let i = 0; i < playerBoss.players; i++ ){
        if( !player.active || !player.alive) continue;
        if( player.team == 1 ){
            if( !player.bot ) teamAHumans++;
            teamAPlayers++;
        } 
        else
        {
            if( !player.bot ) teamBHumans++;
            teamBPlayers++;
        }
    }

    //if adding a human, make sure one team doesn't wind up with all the humans! Keep at worst a 5:1 ratio
    if( !bot ){
        let totalHumans = teamAHumans + teamBHumans; 
        if( totalHumans > 0 ){
            if( teamAHumans < totalHumans / 3 ) return 1;
            if( teamBHumans < totalHumans / 3 ) return 2;
        }
    }

    //now consider the number of players on each team, don't want there to be worse than a 10:1 team ratio
    let totalPlayers = teamAPlayers + teamBPlayers;
    if( totalPlayers > 0 ){
        if( teamAPlayers < totalPlayers / 3 ) return 1;
        if( teamBPlayers < totalPlayers / 3 ) return 2;
    }

    //other wise add based on boid counts
    teamHash.addBoids( boids );
    let a = teamHash.getBoids( 1 ).length;
    let b = teamHash.getBoids( 2 ).length;

    if( a == b ){
        return (Math.random() > 0.5 ? 1 : 2);
    }
    else{
        return a < b ? 1 : 2;
    }
}

function MySQLPushHighScore( inName, inLevel, inScore, inPlaytime, inOnExit ){

    return; // skip until on real server
    if( inPlaytime == 0 ) return;

    let mysqlConnection = mysql.createConnection({
      host     : 'mysql.hyperboid.io',
      user     : 'mhighland',
      password : '8ghsZrKGTG43t9GA',
      database : 'hyperboid_highscores',
    });

    mysqlConnection.connect();

    var post  = {score: inScore, name: inName, level: inLevel, playtime: inPlaytime, onExit: inOnExit };
    var query = mysqlConnection.query('INSERT INTO highscores SET ?', post, function (error, results, fields) {
    if (error) throw error;
        console.log(query.sql); // INSERT INTO posts SET `id` = 1, `title` = 'Hello MySQL' 
    });
   
    mysqlConnection.end();
} 

function compact( value, min, max, compactor ){

    value = clamp( value, min, max );
    let range = max - min;
    return Math.floor( ( value + range/2.0 ) / range * compactor );
}

function decompact( value, min, max, compactor ){

    let range = max - min;
    value *= range / compactor;
    value -= range/2.0;
    return value;
}

function compress16( dataView, offset, value, range )
{
    value = clamp( value, 0, range );
    dataView.setUint16( offset, Math.floor( value / range * 0xFFFF) ); 
}

function decompress16( dataView, offset, range )
{
    let value = dataView.getUint16( offset );
    value *= range / 0xFFFF;
    return value;
}

// helper functions, repeated here because server.js is standalone
function clamp(v, min, max) {
    return Math.max(Math.min(v, max), min);
}

function lerp(t, a, b) {
    return a + t * (b - a);
}

function pilerp(t, a, b) {
    a = modpi( a );
    b = modpi( b );
    if( Math.abs( a - b ) > Math.PI ){
        if( a < 0 ) a += Math.PI * 2.0;
        else b += Math.PI * 2.0;
    }
   return modpi( lerp( t, a, b ) );
}

function modpi( v ) {
    v = v % ( Math.PI * 2 );
    if( v > Math.PI ) v -= Math.PI * 2;
    if( v < -Math.PI ) v += Math.PI * 2;
    return v;
}

function relativeWrap( a, b, size )
{
    if (a instanceof Vector2 && b instanceof Vector2 ) {
            if( a.x > b.x + size * 0.5 ) a.x -= size;
            if( a.x < b.x - size * 0.5 ) a.x += size;
            if( a.y > b.y + size * 0.5 ) a.y -= size;
            if( a.y < b.y - size * 0.5 ) a.y += size;
        }
    else
        throw new Error('Arguments must be of type Vector2'); 
}

function playerDataSortFunction(a,b){
    return b.pop - a.pop;   
}

function boidLifetimeSort(a,b){
    return b.lifetime - a.lifetime;
}
