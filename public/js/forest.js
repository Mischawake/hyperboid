// scrolling background system

class Tree{

    constructor( origin, radius )
    {
        this.root = new THREE.Vector3();
        //this.color = new THREE.Color( 0xffffff * Math.random() );

        this.blend = Math.random();
        let color = teamColors[1].clone();
        color.lerp( teamColors[2], this.blend);
        color.offsetHSL( (Math.random() * 2 - 1) * 0.025, 0, 0);
        this.color = color;

        let theta = Math.random() * Math.PI * 2;
        this.root.x = origin.x + Math.sin( theta ) * radius * Math.random() * 2;
        this.root.z = origin.z + Math.cos( theta ) * radius * Math.random() * 2;
        this.root.y = origin.y;
        this.depth = Math.random() * 50;  



    }

    respawn(origin, radius)
    {
        let theta = Math.random() * Math.PI * 2;
        this.root.x = origin.x + Math.sin( theta ) * (radius + radius * Math.random() );
        this.root.z = origin.z + Math.cos( theta ) * (radius + radius * Math.random() );
        this.root.y = origin.y;
        this.depth = Math.random() * 50;    
        
        let color = teamColors[1].clone();
        color.lerp( teamColors[2], this.blend );
        color.offsetHSL( (Math.random() * 2 - 1) * 0.025, 0, 0);
        this.color = color;
    }

}

class Forest{

    constructor( scene, radius, number ){

        this.radius = radius;
        this.scene = scene;
    
        this.trees = [];
        this.y = 0;
        this.origin = new THREE.Vector3();
        for( let i = 0; i < number; i++ ){
            this.trees[i] = new Tree( this.origin, this.radius );
        }

    }

    update( origin ){

        this.origin = origin;
        for( let i = 0; i < this.trees.length; i++ ){
            let tree = this.trees[i];

            if( origin.distanceTo( tree.root ) > this.radius * 2 )
            {
                tree.respawn( this.origin, this.radius ); 
            }

            let p2 = new THREE.Vector3( lerp( 0.25, tree.root.x, camera.position.x ), tree.root.y - tree.depth, lerp( 0.25, tree.root.z, camera.position.z ) );
            geoBarn.addLine( tree.root, p2, tree.color, new THREE.Color(0x000000) );
        }
    }

}
