let canvas, ctx, scene, renderer, stats, composer;
let camera, cameraLook, cameraLookTarget, cameraFrustum, cameraDiagonal;
let connection;
let input;
let textManager;
let raycaster;
let debugLines;
let debugHud;
//let forest;
let flyerBoss;
let geoBarn;
let seg16;
let playerDataBoss;

let lastAckd;

let bluePercent;
let redPercent;
//
let playerPos;
let playerVel;
let mousePos;


//
let localPlayerID;

let activeBoidCount;

let oldHeart;

let gameTime;

let elapsedTime;

let debugHudVisible;

//cursor
const TAIL_MAX_LENGTH = 2.0;
const TAIL_SEGMENTS = 60.0;

let cursorTrailTimer = 0.0;
let cursorTrailPointer = 0;
let cursorTrail = [];
cursorTrail.length = TAIL_SEGMENTS;

//color short cuts
let color_red = new THREE.Color( 0xff0000 );
let color_green = new THREE.Color( 0x00ff00 );
let color_blue = new THREE.Color( 0x0000ff );
let color_yellow = new THREE.Color( 0xffff00 );
let color_black = new THREE.Color( 0x000000 );
let color_cold = new THREE.Color( 0x3D8FFF );
let color_heat = new THREE.Color( 0xff0427 );
let color_white = new THREE.Color( 0xffffff );
let color_grey = new THREE.Color( 0xAAAAAA );
let color_bg = new THREE.Color( 0x000000 );

//MUST MATCH WITH SERVER!
const BOARD_SIZE = 800;
const BOARD_BORDER = 100;
const MAX_PLAYERS = 50;
const MAX_BOIDS = 2000;
const TO_WIN = 0.66;
const MAX_LIVES = 4;
const POINTS_PER_LEVEL = 10;
const GROWTH_CURVE = 2.5;

const DOMINATION = 250;
// neutral, a, b, background

let teamColors = [ color_grey, color_cold, color_heat, color_bg ];

let StateEnum = {"Title":0, "Signin":1, "Playing":2, "Dead":3 }
Object.freeze(StateEnum);
let clientState = StateEnum.Title;


function init() {
    canvas = document.getElementById('canvas');
    //canvas.style.cursor = "none";
    ctx = canvas.getContext('2d');

    scene = new THREE.Scene();
    
    clientState = StateEnum.Title;

    //set bg color with fog
    //let background_color = teamColors[3];
    //scene.fog = new THREE.Fog( teamColors[3], 2000, 2400);
    //scene.background = new THREE.Color( teamColors[3] );

    //let amb1 = new THREE.AmbientLight(0x000000);
    //scene.add(amb1);

    //let dir1 = new THREE.DirectionalLight(0xffffff, 1.0);
    //dir1.position.set(0, 1, 1);
    //dir1.castShadow = false;
    //scene.add(dir1);

    bluePercent = 0;
    redPercent = 0;

    lastAckd = 0;

    debugHudVisible = false;

    activeBoidCount = 0; 

    // Perspective Camera
    //camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 3500);
    let aspect = window.innerWidth / window.innerHeight;
    cameraFrustum = 200;
    camera = new THREE.OrthographicCamera( cameraFrustum * aspect / - 2, cameraFrustum * aspect / 2, cameraFrustum / 2, cameraFrustum / - 2, 1, 1000 );
    camera.aspect = aspect;
    camera.rotation.x = -Math.PI / 2.0; //looking down
    camera.rotation.y = 0;
    camera.rotation.z = 0;
    scene.add(camera);
    
    cameraLook = new THREE.Vector3(0, 0, 0);
    cameraLookTarget = new THREE.Vector3(0, 0, 0);
    debugLines = new DebugLines( scene );
    debugHud = new DebugHud( document.getElementById("debugHud") );
    input = new Input(); //
    textManager = new TextManager();
    raycaster = new THREE.Raycaster();
    //forest = new Forest(scene, cameraFrustum * 1.5, 400 );
    playerDataBoss = new PlayerDataBoss( MAX_PLAYERS );
    flyerBoss = new FlyerBoss( scene, MAX_BOIDS );
    geoBarn = new GeoBarn( scene );
    seg16 = new Seg16Renderer();

    mousePos = new THREE.Vector2();
    playerPos = new THREE.Vector3();
    playerVel = new THREE.Vector3();
    localPlayerID = 0;

    gameTime = 0;

    for( let i = 0; i < cursorTrail.length; i++ ){
        cursorTrail[i] = new THREE.Vector3();
    }

    //Renderer & Composer

    renderer = new THREE.WebGLRenderer({
        antialias: false
    });

    renderer.setClearColor( teamColors[3] );
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = false;

    composer = new THREE.EffectComposer(renderer);

    //let effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
    //effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight );

    let renderPass = new THREE.RenderPass(scene, camera);

    let bloomPass = new THREE.HyperboidBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight));//1.0, 9, 0.5, 512);
    //let bloomPass = new THREE.TriangleBlurShader();

    let copyShader = new THREE.ShaderPass(THREE.CopyShader);
    copyShader.renderToScreen = true;

    //renderPass.renderToScreen = true;
    composer.addPass(renderPass); 
    //composer.addPass(effectFXAA);   
    composer.addPass(bloomPass);
    composer.addPass(copyShader);

    let container = document.getElementById('webgl-container');
    container.appendChild(renderer.domElement);

    //stats = new Stats();
    //stats.showPanel(0);
    //container.appendChild(stats.dom);

    var host = window.document.location.host;

    connection = new Connection('ws://' + host + '/play');



    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    window.addEventListener('resize', onWindowResize, false);
    onWindowResize();
    window.addEventListener("keypress", onKeyPress, false);

    elapsedTime = 0.0;
}

function onKeyPress( event ) {

    var keyCode = event.keyCode;
    if( keyCode == 32 ){
        if( clientState == StateEnum.Title ){ clientState = StateEnum.Signin; }
        if( clientState == StateEnum.Dead ){ clientState = StateEnum.Title; }
    }

    if( keyCode == 68 ){
        debugHudVisible = !debugHudVisible;
    }

}

function startButton() {
    if( clientState == StateEnum.Signin ) clientState++;
}

function onDocumentMouseMove( event ) {
    mousePos.x = event.clientX; 
    mousePos.y = event.clientY;
}

function onWindowResize() {
    
    let aspect = window.innerWidth / window.innerHeight;
    camera.aspect = aspect;

    let targetArea = cameraFrustum * cameraFrustum;

    camera.left   = - ( cameraFrustum ) / 2;
    camera.right  =   ( cameraFrustum ) / 2;
    camera.top    =   ( cameraFrustum ) / aspect / 2;
    camera.bottom = - ( cameraFrustum  ) / aspect / 2; 

    // keep area consistent:
    let w = camera.right * 2;
    let h = camera.top * 2;
    let area = w * h;
    let zoom = Math.sqrt( targetArea / area );

    zoom = clamp( zoom, 0, 2);

    camera.left *= zoom;
    camera.right *= zoom;
    camera.top *= zoom;
    camera.bottom *= zoom;

    renderer.setSize( window.innerWidth, innerHeight );
    composer.setSize( window.innerWidth, innerHeight );
    canvas.width = window.innerWidth;
    canvas.height = innerHeight;
    camera.updateProjectionMatrix();

    cameraDiagonal = Math.sqrt( Math.pow( camera.right, 2) + Math.pow( camera.top, 2) );

}

let prevTime = 0;

function update(time) {

    if( connection.socket === null )
    {
        document.getElementById("signin").style.display = "none";
        document.getElementById("error").style.display = "block";
        return;
    }

    let dt = (time - prevTime) / 1000;
    prevTime = time;
    dt = clamp( dt, 0.001, 0.1 ); //prevents weirdness when away from tab
    
    elapsedTime += dt;

    let localPlayer = playerDataBoss.getPlayer(localPlayerID); 
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    let snap = flyerBoss.heartSet ? dt * 12 : dt ;

    let cameraRange = (BOARD_SIZE - BOARD_BORDER) / 2;
    cameraLookTarget.x = clamp( cameraLookTarget.x, -cameraRange, cameraRange );
    cameraLookTarget.z = clamp( cameraLookTarget.z, -cameraRange, cameraRange );

    if( cameraLook.distanceTo( cameraLookTarget ) > cameraFrustum/4 ) cameraLook = cameraLookTarget;

    cameraLook = lerpVector3(snap, cameraLook, cameraLookTarget );

    let cameraPos = new THREE.Vector3().addVectors(cameraLook, new THREE.Vector3(0,50,0) );
    camera.position.set(cameraPos.x, cameraPos.y, cameraPos.z);
    camera.updateProjectionMatrix();

    //NETWORKING
    let data = null;

    while ((data = connection.pumpRecv(dt))) {

        let dataView = new DataView(data);
        let offset = 0;
        //local player id
        localPlayerID = dataView.getUint8(offset);
        offset += 1;
        // game time
        gameTime = decompress16( dataView, offset, 2000.0 ); 
        offset += 2;

        lastAckd = dataView.getUint16( offset );
        offset += 2;

        offset = playerDataBoss.deserialize( dataView, offset );

        for( let i = 0; i < MAX_BOIDS; i++ ){
            flyerBoss.flyers[i].active = false;
        }

        // stuff needed for all boids: camera pos, diag, and count
        let serverCamera = new THREE.Vector2(0);
        let serverCameraDiag = 0;

        serverCamera.x = decompact( dataView.getUint16(offset), -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF );
        offset += 2;
        serverCamera.y = decompact( dataView.getUint16(offset), -BOARD_SIZE/2, BOARD_SIZE/2, 0xFFFF );
        offset += 2;
        serverCameraDiag = decompact( dataView.getUint16(offset), 0, BOARD_SIZE * 2.0, 0xFFFF );
        offset += 2;

        //back up - gets overriden by heart pos if set
        cameraLookTarget.set( serverCamera.x, 0, -serverCamera.y );

        activeBoidCount = dataView.getUint16(offset);
        offset += 2; 

        for( let i = 0; i < activeBoidCount; i ++ ){
            
            let t0 = dataView.getUint16(offset);
            let idx = ( t0 & 0xFFF0 ) >>> 4;

            let flyer = flyerBoss.flyers[idx];
            offset = flyer.deserialize( serverCamera, serverCameraDiag, dataView, offset );
            flyer.active = true;                     
        }

    }

    if( flyerBoss.heartSet ){
        cameraLookTarget.set( flyerBoss.heartPos.x, 0, -flyerBoss.heartPos.y );
    }

    debugHud.addLine("Time Remaining: "+gameTime.toFixed(2));
    debugHud.addLine("Boids: "+activeBoidCount);

    let bars = "";
    for( let i = 0; i < connection.recvBuffer.length; i++ ){
        bars +="|";
    }
    debugHud.addLine(bars)
    debugHud.addLine( connection.recvBuffer.length );

    debugHud.addLine( playerDataBoss.teamCounts[1] + "[" + playerDataBoss.teamScores[1] + "] VS " + playerDataBoss.teamCounts[2] + "[" + playerDataBoss.teamScores[2] + "]" );

    playerPos = screenToWorldSpaceXZPlane( mousePos, camera );

    //send player position
    if (connection.pumpSend(dt)) {
        // Serialize our data

        let playerName = document.getElementById("nickname").value;
        let sendName = playerName !== localPlayer.name ? 1 : 0; //only send if name from server doens't match playername

        let interact = input.keyDown(Key.Mouse) || input.keyDown(Key.Space);
        let playerBytes = 2 + 2 + 2 + 1 + 1 + 2 + 1;
        if( sendName ) playerBytes += 16;

        let arrayBuffer = new ArrayBuffer(playerBytes); 
        let dataView = new DataView(arrayBuffer);
        let offset = 0;

        //Camera Data (diag only)
        compress16( dataView, offset, cameraDiagonal, BOARD_SIZE ); 
        offset += 2;

        // Position
        compress16( dataView, offset, playerPos.x + BOARD_SIZE/2, BOARD_SIZE ); 
        offset += 2;
        compress16( dataView, offset, -playerPos.z + BOARD_SIZE/2, BOARD_SIZE ); 
        offset += 2;
        
        // interact and ready
        dataView.setUint8( offset, interact ? 1 : 0 );
        offset += 1;
        dataView.setUint8( offset, clientState );
        offset += 1;
        //
        // last acknowledged
        dataView.setUint16( offset, lastAckd )
        offset +=2;

        //name
        dataView.setUint8(offset, sendName );
        offset += 1;
        if( sendName ){
            let nameLength = playerName.length;
            if( nameLength > 15 ) nameLength = 15; //max 
            dataView.setUint8(offset, nameLength );
            offset += 1;
            for (let x = 0; x < nameLength; x++)
            {
                dataView.setUint8(offset, playerName.charCodeAt(x) );
                offset += 1;
            }
        }

        connection.send(arrayBuffer); 
    }

    flyerBoss.update( dt, localPlayerID );

    //html/css updates
    if(false){
        //visisibility
        let playerOn = localPlayer.pop > 0

        document.getElementById("scoreBoardMaster").style.visibility = "visible";//( playerOn && gameState != 0 ) ? "visible" : "hidden";
        document.getElementById("leaderBoard").style.visibility = "hidden";//( playerOn && gameState != 0 ) ? "visible" : "hidden";
   
        // UI

        let baseCount = -500.0;

        let totalCollected = playerDataBoss.teamScores[1] + playerDataBoss.teamScores[2] + baseCount * 2.0;
        let newRed = ( playerDataBoss.teamScores[1] + baseCount ) / totalCollected;
        newRed = 1.0 - newRed;
        
        newRed = ( newRed - (1.0 - TO_WIN) ) / ( TO_WIN - (1.0 - TO_WIN) ) * 100.0;
        newRed = clamp(newRed,0.0,100.0)

        if( true )
        {
            redPercent = lerp( dt * 2, redPercent, newRed  );
            bluePercent = 100.0 - redPercent;
        }
    }
    //UPDATE LEADER BOARD
    playerDataBoss.updateBoard( dt );
    
    //if( localPlayer.alive == 0 && clientState == StateEnum.Playing ) clientState = StateEnum.Dead;

    //Stateful Interface Stuff

    let signIn = "hidden";

    switch(clientState) {
        case StateEnum.Title:
            drawTitleScreen(cameraPos)
            drawCursor( playerPos, dt );
            break;
        case StateEnum.Signin:
            signIn = "visible";
            drawCursor( playerPos, dt );
            break;
        case StateEnum.Playing:
            drawGameScreen(cameraPos,localPlayer);
            drawCursor( playerPos, dt );
            //drawMiniMap( cameraPos, camera.right, camera.top);
            break;
        case StateEnum.Dead:
            seg16.drawWord( "GAME OVER",new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z), 20.0, color_red, true ); 
            break;
        default:
            
    }

    drawBorders();

    document.getElementById("splashHolder").style.visibility = signIn;

    //stats.update();
    debugHud.flush( debugHudVisible );
    input.flush();
    geoBarn.update();

    //renderer.render( scene, camera );
    renderer.toneMapping = THREE.Uncharted2ToneMapping;
    renderer.toneMappingExposure = 1.0; //adjust how brights the brights are
    composer.render();
    requestAnimationFrame(update);


    // /// math times

    // var s00 = gausian(1.5,1.5);
    // var s10 = gausian(0.5,1.5);
    // var s20 = gausian(2.5,1.5);

    // var s01 = gausian(1.5,0.5);
    // var s11 = gausian(0.5,0.5);
    // var s21 = gausian(2.5,0.5);

    // var s02 = gausian(1.5,2.5);
    // var s12 = gausian(0.5,2.5);
    // var s22 = gausian(2.5,2.5);

    // var sum = s00 + s10 + s20 + s01 + s11 + s12 + s20 + s21 + s22;

    // s00 /= sum;
    // s10 /= sum;
    // s20 /= sum;

    // s01 /= sum;
    // s11 /= sum;
    // s21 /= sum;

    // s02 /= sum;
    // s12 /= sum;
    // s22 /= sum;

    // s00 = Math.round( s00 * 100 );
    // s10 = Math.round( s10 * 100 );
    // s20 = Math.round( s20 * 100 );

    // s01 = Math.round( s01 * 100 );
    // s11 = Math.round( s11 * 100 );
    // s21 = Math.round( s21 * 100 );

    // s02 = Math.round( s02 * 100 );
    // s12 = Math.round( s12 * 100 );
    // s22 = Math.round( s22 * 100 );
   
    // var output = "[" + s02 +"," + s12 +"," + s22 + "]\n";
    // output += "[" + s01 +"," + s11 +"," + s21 + "]\n";
    // output += "[" + s00 +"," + s10 +"," + s20 + "]";
    // console.log(output);
  
}

function drawBorders(){

    let short = 200.0;
    let innerBound = (BOARD_SIZE - BOARD_BORDER);
    let color = color_white.clone();
    color.multiplyScalar(0.1);

    
    debugLines.addLine2D( new THREE.Vector2( innerBound/2, innerBound/2 ), new THREE.Vector2(-innerBound/2, innerBound/2), color );
    debugLines.addLine2D( new THREE.Vector2( -innerBound/2, innerBound/2 ), new THREE.Vector2(-innerBound/2, -innerBound/2), color );
    debugLines.addLine2D( new THREE.Vector2( -innerBound/2, -innerBound/2 ), new THREE.Vector2( innerBound/2, -innerBound/2), color );
    debugLines.addLine2D( new THREE.Vector2( innerBound/2, -innerBound/2 ), new THREE.Vector2( innerBound/2, innerBound/2), color );

    color.multiplyScalar(0.25);

    //UR
    geoBarn.drawFilledRect( new THREE.Vector3( short/2, -25, -(innerBound + short)/2 ), short, short + innerBound, color );

    //LR
    geoBarn.drawFilledRect( new THREE.Vector3( (innerBound + short)/2, -25, short/2 ), short + innerBound, short, color );
      
    //LL
    geoBarn.drawFilledRect( new THREE.Vector3( -short/2, -25, (innerBound + short)/2 ), short, short + innerBound, color ); 

    //UR
    geoBarn.drawFilledRect( new THREE.Vector3( -(innerBound + short)/2, -25, -short/2 ), short + innerBound, short, color );


}

function drawTitleScreen( cameraPos ){

    title_color = color_grey.clone().lerp( color_white, Math.sin( elapsedTime/2.0) / 2.0 + 0.5 );
    seg16.drawWord( "HYPERBOID",new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z), 12.0, title_color, true, 2.0 ); 
    seg16.drawWord( "PRESS SPACE",new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z + camera.top * 0.5), 5.0, color_red, true ); 
    seg16.drawWord( "[C] MJH MMXVII",new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z + camera.top * 0.75), 2.0, color_grey, true ); 

}

function drawPostGameScreen( cameraPos, localPlayer ){

    ui_color = color_green.clone().multiplyScalar(0.5);

    let won = localPlayer.team == 1 && playerDataBoss.teamScores[1] >= playerDataBoss.teamScores[2];
    //to do, tie?

    seg16.drawWord( won ? "YOUR TEAM WON" : "YOUR TEAM LOST",new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z), 12.0, ui_color, true ); 

}

function drawGameScreen( cameraPos, localPlayer )
{

    ui_color = color_green.clone().multiplyScalar(0.5);

    seg16.drawWord( localPlayer.score +"",new THREE.Vector3( cameraPos.x + camera.left + 5.0, 10.0, cameraPos.z - camera.top + 13), 5.0, ui_color, false ); 
    
    let levelText = "LEVEL "+localPlayer.level + " UPGRADE AT " + Math.floor(Math.pow( localPlayer.level, GROWTH_CURVE) * POINTS_PER_LEVEL);
    seg16.drawWord( levelText, new THREE.Vector3( cameraPos.x + camera.left + 5.0, 10.0, cameraPos.z - camera.top + 5), 2.5, ui_color, false ); 
    
    for( let i = 0; i < localPlayer.lives; i++ ){
        let p = new THREE.Vector3( cameraPos.x + camera.left + 7.0 + 5 * i, 10.0, cameraPos.z - camera.top + 20);
        geoBarn.drawFlyer( p, 2.5, 0, ui_color, false );
        geoBarn.drawFlyer( p, 2.5, 0, ui_color.clone().multiplyScalar(0.25), true );
    }

    seg16.drawWord( "TEAM RANK " + localPlayer.rank +"/", new THREE.Vector3( cameraPos.x + camera.left + 5.0, 10.0, cameraPos.z + camera.top - 5), 2.5, ui_color, false ); 

    let minutes = Math.floor( gameTime / 60.0 );
    let seconds = Math.floor ( gameTime - ( minutes * 60 ) );
    if( seconds < 10 ) seconds = "0"+seconds;

    let time = ""+seconds;
    if( minutes > 0 ) time = minutes+":"+time;

    let w = 2.0;
    let h = 4.0;
    let bars = 25;

    let center = new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z - camera.top + 10)
    center.x -= w * bars * 0.5;
    center.z -= 5;

    for(let i = 0; i <= bars; i ++ ){

        let col = i/bars < playerDataBoss.blueDom ? teamColors[1].clone() : teamColors[2].clone();

        geoBarn.addLine( new THREE.Vector3( center.x, center.y, center.z + h/2 ), new THREE.Vector3( center.x, center.y, center.z - h/2 ), col, col );
        center.x += w;
    }


    seg16.drawWord( time, new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z - camera.top + 13), 2.5, ui_color, true ); 



    // seg16.drawWord( ""+playerDataBoss.teamScores[1] , new THREE.Vector3( cameraPos.x + 25, 10.0, cameraPos.z - camera.top + 40), 4.0, teamColors[1], true ); 

    // seg16.drawWord( ""+playerDataBoss.teamScores[2] , new THREE.Vector3( cameraPos.x - 25, 10.0, cameraPos.z - camera.top + 40), 4.0, teamColors[2], true ); 


    if( !localPlayer.alive ){
        seg16.drawWord( "WAITING TO SPAWN",new THREE.Vector3( cameraPos.x, 10.0, cameraPos.z), 4.0, ui_color, true ); 
    }


    /*
     let baseCount = -500.0;

        let totalCollected = playerDataBoss.teamScores[1] + playerDataBoss.teamScores[2] + baseCount * 2.0;
        let newRed = ( playerDataBoss.teamScores[1] + baseCount ) / totalCollected;
        newRed = 1.0 - newRed;
        
        newRed = ( newRed - (1.0 - TO_WIN) ) / ( TO_WIN - (1.0 - TO_WIN) ) * 100.0;
        newRed = clamp(newRed,0.0,100.0)

        if( true )
        {
            redPercent = lerp( dt * 2, redPercent, newRed  );
            bluePercent = 100.0 - redPercent;
        }
    */


}


function drawCursor( pos, dt ){

    //pointer always points to current positon!
    // cursorTrailTimer += dt;
    // if( cursorTrailTimer > TAIL_MAX_LENGTH / TAIL_SEGMENTS )
    // {
    //     cursorTrailPointer = (cursorTrailPointer + 1 ) % cursorTrail.length;
    //     cursorTrailTimer = 0;
    // }

    // cursorTrail[ cursorTrailPointer ].copy( pos );

    // for( let i = 0; i < cursorTrail.length; i++ ){

    //     let p = cursorTrail[i];

    //     geoBarn.addLine( new THREE.Vector3( p.x+1.0, 30.0, p.z), 
    //                  new THREE.Vector3( p.x-1.0, 30.0, p.z),
    //                  color_green,
    //                  color_green);

    //     geoBarn.addLine( new THREE.Vector3( p.x, 30.0, p.z+1.0), 
    //                  new THREE.Vector3( p.x, 30.0, p.z-1.0),
    //                  color_green,
    //                  color_green);

    // }

    geoBarn.addLine( new THREE.Vector3( pos.x+2.0, 30.0, pos.z+2.0), 
                     new THREE.Vector3( pos.x-2.0, 30.0, pos.z-2.0),
                     color_green,
                     color_green);
     geoBarn.addLine( new THREE.Vector3( pos.x-2.0, 30.0, pos.z+2.0), 
                     new THREE.Vector3( pos.x+2.0, 30.0, pos.z-2.0),
                     color_green,
                     color_green);
}

function drawColorGuide(center,color){

    let w = 1.5;
    let h = 4;
    let bars = 120;

    //center.x -= w * bars / 2.0;

     //geoBarn.drawRect( new THREE.Vector3( center.x + 20, center.y, center.z ), 10, 10, new THREE.Color(0x00FF00) );
     geoBarn.drawFilledRect( new THREE.Vector3( center.x - 20, center.y, center.z ), 10, 10, color_white );


    for(let i = 0; i < bars; i ++ ){

        let col = color.clone();

        col.multiplyScalar( i / (bars-1) );

        //col.copyGammaToLinear(col, 2.2);

        //geoBarn.addLine( new THREE.Vector3( center.x, center.y, center.z + h/2 ), new THREE.Vector3( center.x, center.y, center.z - h/2 ), col, col );

       
        center.x += w;
    }




}

function drawMiniMap( cam, camRight, camTop ){

    let center = new THREE.Vector3( cam.x, 20.0, cam.z);



    let width = 33.0;
    let height = 33.0;

    center.x -= camRight - width/2 - 2;
    center.z += camTop - height/2 - 2;

   
    //geoBarn.drawRect( center, width, height, color_green );


    for( let i = 0; i < playerDataBoss.players.length; i++ ){

        let player = playerDataBoss.players[i];

        if( player.active == false ) continue;

        if( player.pop == 0 ) continue;

        let scaledX = player.heartPos.x / BOARD_SIZE * width;
        let scaledY = player.heartPos.y / BOARD_SIZE * height;

        //addCircle( center, radius, normal, color, segments = 18 )
        let radius = 0.1 + player.pop/25.0;
        let color = teamColors[player.team];
        if( i == localPlayerID ){
            color = color_white
            radius = 3.0;
            geoBarn.drawRect( center.clone().add( new THREE.Vector3( scaledX, 10 - radius, -scaledY)), radius, radius, color );
        } 
        else
        {
            geoBarn.drawRect( center.clone().add( new THREE.Vector3( scaledX, 10 - radius, -scaledY)), radius, radius, color );

        }


        
        
    }    



}



function gausian(x,y){

    return Math.exp( -0.4*( x*x + y*y ) );

}

window.onload = () => {
    init();
    update(0);
};
