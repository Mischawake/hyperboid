class SColor{



	constructor( r, g, b, l ){
			
		//0-1s
		this.r = r;
		this.g = g;
		this.b = b;
		this.l = l;

	}

	GetColor(){

		let c = new THREE.Color();


		let max = Math.max( this.r, Math.max( this.g, this.b ) );

		c.r = (this.r / max) * this.l;
		c.g = (this.g / max) * this.l;
		c.b = (this.b / max) * this.l;

		c.r = Math.pow( c.r, 2.2 );
		c.g = Math.pow( c.g, 2.2 );
		c.b = Math.pow( c.b, 2.2 );



		return c;

	}

}