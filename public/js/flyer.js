//const TAIL_SEGMENTS = 4;
//const TAIL_MAX_LENGTH = TAIL_SEGMENTS * 0.1;
const MAX_FRAGMENTS = 500;

let flyerScales = [0.0,1.25, 2.5, 4.0];

class FlyerBoss{

	constructor( scene, flyerCount ){
			
		this.heartSet = false;
		this.heatPos = new THREE.Vector2();

		this.flyers = [];
		this.flyers.length = flyerCount;

		this.fragments = [];
		this.fragments.length = MAX_FRAGMENTS;
		this.fragmentPointer = 0;

		for( let i = 0; i < this.flyers.length; i++ )
		{
			this.flyers[i] = new Flyer( scene );
		}

		for( let i = 0; i < this.fragments.length; i++ )
		{
			this.fragments[i] = new Fragment();
		}

		this.pulse = 0.0;
		this.pulseCount = 0.0;
	}

	update( dt, localID ){

		this.pulseCount += dt;
		this.pulse = (Math.sin( this.pulseCount * 10.0 ) + 1.0) * 0.5;

		let localPlayer = playerDataBoss.getPlayer( localID );

		this.heartSet = false;

		for( let i = 0; i < this.flyers.length; i++ )
		{
			let flyer = this.flyers[i];

			if( !flyer.active ){
				flyer.wasActive = false;
				continue;
			} 

			flyer.update( dt ); 

			if( flyer.isKing && flyer.family == localID ){
				this.heartSet = true;
				this.heartPos = flyer.pos;
			}

			//to do, not sure this is fixing z fighting
			let y = ( flyer.pos.x + flyer.pos.y ) / (BOARD_SIZE);
			y += flyer.state;

			let worldPos = new THREE.Vector3( flyer.pos.x, y, -flyer.pos.y );

			let player = playerDataBoss.getPlayer( flyer.family );

        	if( flyer.alive || flyer.spawnFragment )
        	{
        		
        		if( flyer.team !== 0 && flyer.family !== localID || flyer.state !== 3 )
        		{
        			//color.offsetHSL( flyer.colorVariation * 0.025, -0.36, 0);
        		}

				var rotMatrix = new THREE.Matrix3();
				let cos = Math.cos( flyer.angle );
				let sin = Math.sin( flyer.angle );

				rotMatrix.set(  cos, 0, sin,
	       						0, 1, 0,
	       				       -1 * sin, 0, cos );

				let p1 = new THREE.Vector3( 0,  0, -1.0 );
		 	    let p2 = new THREE.Vector3( -0.7071, 0.0, 0.7071 );
				let p3 = new THREE.Vector3( 0, 0, 1.0 );
				let p4 = new THREE.Vector3( 0.7071, 0.0, 0.7071 );

				//reposition center
				let comZ = (p1.z + p2.z + p3.z + p4.z) / 4.0;
				p1.z -= comZ;
				p2.z -= comZ;
				p3.z -= comZ;
				p4.z -= comZ;

				let length = p1.length();

				p1.applyMatrix3( rotMatrix );
				p2.applyMatrix3( rotMatrix );
				p3.applyMatrix3( rotMatrix );
				p4.applyMatrix3( rotMatrix );

				//don't need all this
				let n1 = new THREE.Vector3().crossVectors( p1.clone().sub(p3), p4.clone().sub(p1) ).normalize();
				let n2 = new THREE.Vector3().crossVectors( p1.clone().sub(p2), p3.clone().sub(p1) ).normalize();

				p1.multiplyScalar( flyer.scale );
				p2.multiplyScalar( flyer.scale );
				p3.multiplyScalar( flyer.scale );
				p4.multiplyScalar( flyer.scale );

				length *= flyer.scale;

				let a = new THREE.Vector3();
				let b = new THREE.Vector3();

				//draw solid inside

				let solidColor =  flyer.color.clone().multiplyScalar(0.05);
				
				let lineColor = flyer.color.clone();
				
				if( player.alive == 1 && flyer.team !== 0 ){
					lineColor.lerp( color_black, this.pulse * 0.75 );
				}

				if( flyer.team !== 0 ){
					//solid mode
					
					geoBarn.addVert( a.addVectors(p1,worldPos), solidColor, n1 );
					geoBarn.addVert( a.addVectors(p2,worldPos), solidColor, n1 );
					geoBarn.addVert( a.addVectors(p3,worldPos), solidColor, n1 );
					
					geoBarn.addVert( a.addVectors(p1,worldPos), solidColor, n2 );
					geoBarn.addVert( a.addVectors(p3,worldPos), solidColor, n2 );
					geoBarn.addVert( a.addVectors(p4,worldPos), solidColor, n2 );
				}

				if( flyer.spawnFragment ){
				
					this.spawnFragment( a.addVectors(p1,worldPos), b.addVectors(p2,worldPos), worldPos, flyer.color, flyer.vel );
					this.spawnFragment( a.addVectors(p2,worldPos), b.addVectors(p3,worldPos), worldPos, flyer.color, flyer.vel );
					this.spawnFragment( a.addVectors(p3,worldPos), b.addVectors(p4,worldPos), worldPos, flyer.color, flyer.vel );
					this.spawnFragment( a.addVectors(p3,worldPos), b.addVectors(p1,worldPos), worldPos, flyer.color, flyer.vel );
		
				}
				else
				{
					geoBarn.addLine( a.addVectors(p1,worldPos), b.addVectors(p2,worldPos), lineColor, lineColor);
					geoBarn.addLine( a.addVectors(p2,worldPos), b.addVectors(p3,worldPos), lineColor, lineColor );
					geoBarn.addLine( a.addVectors(p3,worldPos), b.addVectors(p4,worldPos), lineColor, lineColor );
					geoBarn.addLine( a.addVectors(p4,worldPos), b.addVectors(p1,worldPos), lineColor, lineColor );
				}

				//geoBarn.addLine( new THREE.Vector3( flyer.targetPos.x, 0, -flyer.targetPos.y ), worldPos, color_green, color_green );
				//geoBarn.drawFlyer( new THREE.Vector3( flyer.targetPos.x, 0, -flyer.targetPos.y ), flyer.scale - 1, flyer.targetAngle, color_green, false );
				//drawFlyer( worldPos, scale, angle, color, solid ){

				//looks good - should use same decoration (tiny gold boid) in post game screen
				if( flyer.isKing && player.rank == 1 && player.pop >= 10 ){
					//team captain

					solidColor.set( color_yellow );

					let lilp1 = p1.clone().multiplyScalar(0.33);
					let lilp2 = p2.clone().multiplyScalar(0.33);
					let lilp3 = p3.clone().multiplyScalar(0.33);
					let lilp4 = p4.clone().multiplyScalar(0.33);

					worldPos.y += 0.1;

					geoBarn.addVert( a.addVectors(lilp1,worldPos), solidColor, n1 );
					geoBarn.addVert( a.addVectors(lilp2,worldPos), solidColor, n1 );
					geoBarn.addVert( a.addVectors(lilp3,worldPos), solidColor, n1 );
					
					geoBarn.addVert( a.addVectors(lilp1,worldPos), solidColor, n2 );
					geoBarn.addVert( a.addVectors(lilp3,worldPos), solidColor, n2 );
					geoBarn.addVert( a.addVectors(lilp4,worldPos), solidColor, n2 );
				
				}

				//decorations
				if( flyer.wasKing && player.level > 1 ){

					let binaryString = ( (player.level-1) >>> 0).toString(2);
					
					binaryString = binaryString.split("").reverse().join("");

					for( let x = 0; x < binaryString.length; x++ ){

						//let lineColor = flyer.color.clone();

						if( binaryString[x] == 0 ) lineColor.multiplyScalar(0.15);

						let inc = 2.0;
						p1.multiplyScalar( (length + inc)/length );
						p2.multiplyScalar( (length + inc)/length );
						p3.multiplyScalar( (length + inc)/length );
						p4.multiplyScalar( (length + inc)/length );

						length += inc;		

						//needs to be based on state changing from 3 to 2
						if( !flyer.isKing ){
					
							this.spawnFragment( a.addVectors(p1,worldPos), b.addVectors(p2,worldPos), worldPos, lineColor, flyer.vel);
							this.spawnFragment( a.addVectors(p2,worldPos), b.addVectors(p3,worldPos), worldPos, lineColor, flyer.vel);
							this.spawnFragment( a.addVectors(p3,worldPos), b.addVectors(p4,worldPos), worldPos, lineColor, flyer.vel);
							this.spawnFragment( a.addVectors(p3,worldPos), b.addVectors(p1,worldPos), worldPos, lineColor, flyer.vel);
			
						}
						else
						{
							geoBarn.addLine( a.addVectors(p1,worldPos), b.addVectors(p2,worldPos), lineColor, lineColor );
							geoBarn.addLine( a.addVectors(p2,worldPos), b.addVectors(p3,worldPos), lineColor, lineColor );
							geoBarn.addLine( a.addVectors(p3,worldPos), b.addVectors(p4,worldPos), lineColor, lineColor );
							geoBarn.addLine( a.addVectors(p4,worldPos), b.addVectors(p1,worldPos), lineColor, lineColor );

						}
					}
				}
				
				//color.copy( teamColors[ flyer.team ] );
				//TAIL
				// if( flyer.state == 1 && flyer.team != 0 )
				// {
				// 	let cur = flyer.tail[ flyer.tailPointer ].clone();
					
				// 	let numSegments = flyer.tail.length;
				// 	for( let i = 0; i < numSegments - 1; i++ )
				// 	{
				// 		let idx = (flyer.tailPointer - i - 1);
				// 		if( idx < 0 ) idx += flyer.tail.length;

				// 		let next = flyer.tail[ idx ].clone();

				// 		if( i === numSegments - 2 ){
				// 			//tail fix so no pop
				// 			let lerp = clamp( flyer.tailTimer / ( TAIL_MAX_LENGTH / TAIL_SEGMENTS ), 0, 1 );
				// 			next.lerp( cur, lerp );
				// 		}

				// 		let curColor = flyer.color.clone();
				// 		let nextColor = flyer.color.clone();

				// 		let curLerp = (i * ( TAIL_MAX_LENGTH / TAIL_SEGMENTS ) + flyer.tailTimer ) / TAIL_MAX_LENGTH;
				// 		let nextLerp = ( (i + 1) * ( TAIL_MAX_LENGTH / TAIL_SEGMENTS ) + flyer.tailTimer ) / TAIL_MAX_LENGTH;
				// 		curLerp = clamp( curLerp, 0, 1 );
				// 		nextLerp = clamp( nextLerp, 0, 1 );

				// 		curLerp *= curLerp;
				// 		nextLerp *= nextLerp;

				// 		curColor.lerp( new THREE.Color( 0x000000 ), curLerp );
				// 		nextColor.lerp( new THREE.Color( 0x000000 ), nextLerp );
				// 		geoBarn.addLine( new THREE.Vector3( cur.x, 0, -cur.y), new THREE.Vector3( next.x, 0, -next.y), curColor, nextColor );
				// 		cur.copy( next );
						
				// 	}
				//}
			}

			//A Heart, add a name

	        if( flyer.state === 3 )
	        {	
			
				let name = player.name;
		
				worldPos.x += 3;
				worldPos.z -= 5;

				seg16.drawWord( player.lives + "", worldPos, 1.5, flyer.color ); 
    
			}			
			

		}

		//fragments
		for( let i = 0; i < this.fragments.length; i++ )
		{
			let frag = this.fragments[i];
			if( !this.fragments[i].active ) continue;
			
			frag.update(dt);

			geoBarn.addLine( frag.start, frag.end, frag.color, frag.color );
		}


	}

	spawnFragment(a,b,origin, color, vel = new THREE.Vector2(0) ){

		this.fragments[ this.fragmentPointer ].spawn(a,b,origin, vel, color);
		this.fragmentPointer++;
		if( this.fragmentPointer == MAX_FRAGMENTS ){ this.fragmentPointer = 0; }

	}


}

class Fragment
{
	constructor(){
		this.start = new THREE.Vector3();
		this.end = new THREE.Vector3();
		this.vel = new THREE.Vector2();
		this.rotVel = 0.0;
		this.active = false;
		this.color = new THREE.Color();
	}

	spawn( a, b, origin, vel, color ){
		//vec 3, vec3, vec3, vec2, color

		this.start = a.clone();
		this.end = b.clone();
		this.vel.x = vel.x * 0.5;
		this.vel.y = vel.y * 0.5;
		
		this.vel.x += (Math.random() * 2 - 1) * 4.0;
		this.vel.y += (Math.random() * 2 - 1) * 4.0;

		this.color.set(color);
		this.active = true;
	}

	update(dt){

		if( this.start.distanceToSquared(this.end) < 0.05 ){
			this.active = false;
		} 

		if( !this.active ) return;

		this.start.lerp( this.end, dt / 10.0);
		this.end.lerp( this.start, dt / 10.0);

		this.color.lerp( color_black, dt / 20.0);

		this.vel.lerp( new THREE.Vector2(0,0), dt / 4.0 );

		let delta = this.vel.clone().multiplyScalar( dt );

		this.start.x += delta.x;
		this.start.z -= delta.y;
		this.end.x += delta.x;
		this.end.z -= delta.y;

	}

}


class Flyer
{
	constructor( scene ){
		this.pos = new THREE.Vector2();
		this.angle = 0;
		this.targetPos = new THREE.Vector2();
		this.targetAngle = 0;
		this.vel = new THREE.Vector2();
		this.targetVel = new THREE.Vector2();
		this.family = 0;
		this.team = 0;
		//this.lastTeam = 0;
		this.scale = 0;
		this.alive = false;

		this.spawnFragment = false;

		this.wasAlive = false;
		this.wasKing = false;
		this.isKing = false; 	

		this.active = false;
		this.state = 0;
		this.wasActive = false;
		
		this.color = new THREE.Color();

		this.colorVariation = Math.random() * 2 - 1;
		
		// this.tail = [];
		// this.tail.length = TAIL_SEGMENTS;
		// for( let i = 0; i < this.tail.length; i++ ){
		// 	this.tail[i] = new THREE.Vector2();
		// }
		// this.tailPointer = 0;
		// this.tailTimer = 0;

	}

	deserialize( origin, range, dataView, offset ){    

		/*
        10 bytes per boid
        t0) 16: id(12) team(4)
        t1) 32: family (8) state (2) posx (11) posy (11)
        t2) 32: rotation (10) velx (11) vely (11) 
        */

        let t0 = dataView.getUint16(offset);
        offset += 2;
        let t1 = dataView.getUint32(offset);
        offset += 4;
        let t2 = dataView.getUint32(offset);
        offset += 4;

        //this.lastTeam = this.team;
        this.team = ( t0 & 0xF );        
        this.family = ( t1 & 0xFF000000 ) >>> 24;
        this.state = ( t1 & 0xC00000 ) >>> 22;

        let posXOut = ( t1 & 0x3FF800 ) >>> 11;
        let posYOut = ( t1 & 0x7FF );
        
        let rotationOut = ( t2 & 0xFFC00000 ) >>> 22;
        let velXOut = ( t2 & 0x3FF800 ) >>> 11;
        let velYOut = ( t2 & 0x7FF );

        let velRange = 100.0;

        let x  = origin.x + decompact( posXOut, -range, range, 0x7FF );
        let y = origin.y + decompact( posYOut, -range, range, 0x7FF );
        let r = decompact( rotationOut, -Math.PI, Math.PI, 0x3FF );
        let vx = decompact( velXOut, -velRange/2, velRange/2, 0x7FF );
        let vy = decompact( velYOut, -velRange/2, velRange/2, 0x7FF );

        this.setTargetPos( x, y );
        this.setTargetAngle( r ); 
        this.setTargetVel( vx, vy );

        return offset;
	}

	update(dt) {

		this.alive = this.state > 0;
		this.isKing = this.state == 3;
		this.spawnFragment = false;

		//just turned on or spawned
		if( !this.wasActive || (!this.wasAlive && this.alive) ){

			this.scale = flyerScales[ this.state ];
			this.pos.copy( this.targetPos );
			this.angle = this.targetAngle;
	
		}
		else{
     
        	this.pos.lerp( this.targetPos, dt * 4.0 );
			this.angle = pilerp(  dt * 12.0 , this.angle, this.targetAngle );
			this.vel.lerp( this.targetVel, dt * 12.0 );
			this.pos.add( this.vel.clone().multiplyScalar( dt ) ); // add to both
			
			this.scale = lerp( dt * 2, this.scale, flyerScales[this.state] );
			// add vel to target pos too
			this.targetPos.add( this.targetVel.clone().multiplyScalar( dt ) );

			this.targetVel.lerp( new THREE.Vector2(), dt * 1.0 );	
			//debugLines.addCircle( this.targetPos, 1.0, )

		}

	
		if( this.wasAlive && !this.alive ){
			this.spawnFragment = true;
		}
		else{
			this.color.set( teamColors[ this.team ] );
		}

		this.wasActive = true;
		this.wasAlive = this.alive;
		this.wasKing = this.isKing;
    
		// pointer always points to current positon!
		// this.tailTimer += dt;
		// if( this.tailTimer > TAIL_MAX_LENGTH / TAIL_SEGMENTS )
		// {
		// 	this.tailPointer = (this.tailPointer + 1 ) % this.tail.length;
		// 	this.tailTimer = 0;
		// }
		// this.tail[ this.tailPointer ].copy( this.pos );

	}

	setTargetPos( x, y ) {

		this.targetPos.x = x;
		this.targetPos.y = y;
	}

	setTargetVel( x, y )
	{
		this.targetVel.x = x;
		this.targetVel.y = y;
	}

	setTargetAngle( target ) {
		this.targetAngle = target;
	}

}

