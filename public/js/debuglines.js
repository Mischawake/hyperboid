// adds lines of text to a given container, must be called each frame
class DebugHud
{
	constructor( container )
	{
		this.content = [];
		this.idx = 0;
		this.container = container;
	}

	addLine( text )
	{
		this.content[ this.idx ] = text;
		this.idx++;
	}

	flush( visible )
	{
		if( !visible ){
			this.container.innerHTML = "";
		}
		else{
			let text = "DebugHud";
			for( let i = 0; i < this.content.length; i++ )
			{
				text += "<br>" + this.content[i];
			}
			this.container.innerHTML = text;
		}
		
		this.content = [];
		this.idx = 0;
	}
}

class DebugLines
{
	constructor( scene )
	{
		
	}

	addLine2D(a, b, color)
	{
		geoBarn.addLineVertex( new THREE.Vector3(a.x, 0, -a.y ), color );
		geoBarn.addLineVertex( new THREE.Vector3(b.x, 0, -b.y ), color );
	}

	addArrow(a, b, radius, color )
	{
		geoBarn.addLineVertex( a, b, color );

		let length = a.distanceTo( b );
		radius = length * radius / 2.0;
		let bToA = new THREE.Vector3().subVectors( a, b ).normalize();
		let tipCenter = new THREE.Vector3().addVectors( b, bToA.clone().multiplyScalar( radius * 2.0 ) );

		let x = bToA.clone();
		let y = new THREE.Vector3();
		let z = new THREE.Vector3();

		if( Math.abs(x.x) >= 0.57735 )
			y.set( x.y, -x.x, 0.0 );
		else 
			y.set( 0.0, x.z, -x.y );

		x.normalize();
		z.crossVectors( x, y );

		let segments = 18;
		let lastPoint = new THREE.Vector3();
		let newPoint = new THREE.Vector3();
	
		for( let i = 0; i <= segments; i++ )
		{	
			let theta = Math.PI * 2 / segments * i;
			newPoint.copy( tipCenter );
			newPoint.add( y.clone().multiplyScalar( Math.sin( theta ) * radius ) );
			newPoint.add( z.clone().multiplyScalar( Math.cos( theta ) * radius ) );
			if( i > 0 ) geoBarn.addLine( lastPoint, newPoint, color );
			
			if( i % 3 <= 0) geoBarn.addLineVertex( newPoint, b, color );
			lastPoint.copy( newPoint );
		}
	}

	addSphere( center, radius, color )
	{
		this.addCircle( center, radius, new THREE.Vector3(1,0,0), color );
		this.addCircle( center, radius, new THREE.Vector3(0,1,0), color );
		this.addCircle( center, radius, new THREE.Vector3(0,0,1), color );
	}

	addCircle( center, radius, normal, color, segments = 18 )
	{
		let a = normal.normalize();
		let b = new THREE.Vector3();
		let c = new THREE.Vector3();

		if( Math.abs(a.x) >= 0.57735 )
			b.set( a.y, -a.x, 0.0 );
		else 
			b.set( 0.0, a.z, -a.y );

		b.normalize();
		c.crossVectors( a, b );

		let lastPoint = new THREE.Vector3();
		let newPoint = new THREE.Vector3();
	
		for( let i = 0; i <= segments; i++ )
		{	
			let theta = Math.PI * 2 / segments * i;
			newPoint.copy( center );
			newPoint.add( b.clone().multiplyScalar( Math.sin( theta ) * radius ) );
			newPoint.add( c.clone().multiplyScalar( Math.cos( theta ) * radius ) );
			if( i > 0 ) geoBarn.addLine( lastPoint, newPoint, color, color );
			lastPoint.copy( newPoint );
		}
	}

	


}