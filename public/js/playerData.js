class PlayerData{

	constructor(){
		this.pop = 0;
		this.kills = 0;
		this.score = 0;
		this.active = false;
		this.name = "Unnamed";
		this.bot = false;
		this.team = 0;
		this.level = 1;
		this.lives = 0;
		this.rank = 0;
		this.alive = 0;
		this.id = 0;
	}

}

class PlayerDataBoss{

	constructor( numPlayers ){
		this.players = [];
		for( let i = 0; i<numPlayers; i++ )
		{
			this.players[i] = new PlayerData();
			this.players[i].id = i;
		}	

		this.teamScores = [];

		this.teamCounts = [];

		this.teamScores.length = 3; // should be dynamic eventually
		this.teamScores.fill( 0 );

		this.teamCounts.length = 3; // should be dynamic eventually
		this.teamCounts.fill( 0 );

		this.blueDom = 0.5;

	}

	getPlayer( idx ){
		return this.players[idx];
	}

	deserialize( dataView, offset ){
		
		let numStates = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numStates; i++ ){
			let id = dataView.getUint8( offset );
			offset += 1;
			let state =  dataView.getUint8( offset );
			offset += 1;
			this.players[id].active = (state > 0);
			this.players[id].bot = (state === 2);
		}

		let numTeams = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numTeams; i++ ){
			let id = dataView.getUint8( offset );
			offset += 1;
			let team =  dataView.getUint8(offset);
			offset += 1;
			this.players[id].team = team;
		}

		//pops
		let numPops = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numPops; i++ ){
			let id = dataView.getUint8( offset );
			offset += 1;
			let pop =  dataView.getUint16(offset);
			offset += 2;
			this.players[id].pop = pop;
		}

		//scores
		let numScores = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numScores; i++ ){
			let id = dataView.getUint8( offset );
			offset += 1;
			let score =  dataView.getUint16(offset);
			offset += 2;
			this.players[id].score = score;
		}

		//lives
		let numLives = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numLives; i++ ){
			let id = dataView.getUint8( offset );
			offset += 1;
			let lives =  dataView.getUint8(offset);
			offset += 1;
			this.players[id].lives = lives;
		}

		//names
		let numNames = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numNames; i++ ){
			let id = dataView.getUint8( offset );
			offset += 1;

			let nameLength =  dataView.getUint8(offset);
			offset += 1;
			let name = "";
			for (let x = 0; x < nameLength; x++){
					name += String.fromCharCode( dataView.getUint8(offset) );
					offset += 1;
			}
			this.players[id].name = name;
		}

		//ranks
		let numRanks = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numRanks; i++ ){

				let id = dataView.getUint8( offset );
				offset += 1;
				let rank =  dataView.getUint8(offset);
				offset += 1;
				this.players[id].rank = rank;
		}

		//levels
		let numLevels = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numLevels; i++ ){

			let id = dataView.getUint8( offset );
			offset += 1;
			let level =  dataView.getUint8(offset);
			offset += 1;
			this.players[id].level = level;
		}

		//alive
		let numAlives = dataView.getUint8( offset );
		offset += 1;
		for( let i = 0; i < numAlives; i++ ){

			let id = dataView.getUint8( offset );
			offset += 1;
			let alive = dataView.getUint8(offset);
			offset += 1;
			this.players[id].alive = alive;
		}

		return offset;
	}

	getPlayerRank(id)
	{
		return this.players[id].rank;
	}

	getPlayerName(id)
	{
		return this.players[id].name;
	}

	getPlayerScore(id)
	{
		return this.players[id].score;
	}

	updateBoard( dt ){

		this.teamScores.fill( 0 );
		this.teamCounts.fill( 0 );

		let count = 0;
		let botCount = 0;

		for( let i = 0; i < this.players.length; i++ ){
			let player = this.players[i];
			if( player.active ) count++;
			if( player.bot ) botCount++;
			if( player.active && player.pop > 0 ){ 
				this.teamScores[ player.team ] += player.pop;
				this.teamCounts[ player.team ] += 1;
			}    
		}

    	let newBlueDom = clamp( ( playerDataBoss.teamScores[1] - playerDataBoss.teamScores[2] ) / DOMINATION, -1.0, 1.0 );
    	newBlueDom = ( newBlueDom + 1.0 ) / 2.0;

    	this.blueDom = meet( dt * 0.5, this.blueDom, newBlueDom );


		debugHud.addLine( "Humans: " + (count - botCount) + " Bots: " + botCount );
	}

	getPlayersSorted(){

		let playersRanked = this.players.slice();
		playersRanked.sort( playerDataSortFunction );
		return playersRanked;
	}		
	
}

function playerDataSortFunction(a,b){
	return b.pop - a.pop;	
}
