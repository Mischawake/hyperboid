class Text {
    constructor(text, pos, scale, color) {
        this.text = text;
        this.pos = pos;
        this.scale = scale;
        this.color = color;
    }

    update(dt, ctx, camera) {
        let pos2d = worldToScreenspace(this.pos.clone(), camera);
        let textScale = 12 * this.scale;
        ctx.font = `bold ${textScale}px sans-serif`;
        ctx.lineWidth = 2;
        let textWidth = ctx.measureText(this.text).width;
        let textHeight = textScale;
        let textPos = pos2d.sub(new THREE.Vector2(textWidth * 0.5,
                                                  -textHeight * 0.3));
        ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
        ctx.fillText(this.text, textPos.x + 1, textPos.y + 1);
        ctx.fillStyle = this.color.getStyle();
        ctx.fillText(this.text, textPos.x, textPos.y);
    }
}

class TextManager {
    constructor() {
        this.texts = [];
    }

    addText(text, duration) {
        this.texts.push({
            text: text,
            expires: duration > 0,
            expireTime: new Date().getTime() + duration * 1000
        });
    }

    removeText(text) {
        let idx = this.texts.indexOf(text);
        if (idx >= 0) {
            this.texts.splice(idx, 1);
        }
    }

    update(dt, ctx, camera) {
        let time = new Date().getTime();
        for (let i = 0; i < this.texts.length; i++) {
            let t = this.texts[i];
            t.text.update(dt, ctx, camera);
            if (t.expires && t.expireTime < time) {
                this.texts.splice(i, 1);
                i--;
            }
        }
    }
}
