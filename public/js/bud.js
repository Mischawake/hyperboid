class Bud {
	
	constructor( scene ){
		this.pos = new THREE.Vector2();
		this.radius = 0.7;
		this.state = 0;
		this.color = new THREE.Color().setHSL( Math.random(), 0.3, 0.6 );
	}

	deserialize( dataView, offset ){
		 //state, x, y, radius
        let state = dataView.getUint8(offset);
        offset += 1;
        let x = dataView.getFloat32(offset);
        offset += 4;
        let y = dataView.getFloat32(offset);
        offset += 4; 
        let r = dataView.getFloat32(offset);
        offset += 4;    
        this.pos.x = x;
        this.pos.y = y;
        this.radius = r;
        this.state = state;
        return offset;
	}

	update(dt, camera, boardsize ) {

		let visible = this.state > 0;

		if( visible )
		{
			let flatCamera = new THREE.Vector2(camera.position.x, -camera.position.z );
			let pos = this.pos.clone();

			if( WRAP_BOARD )
			{
				if( pos.x > flatCamera.x + boardsize * 0.5 ) pos.x -= boardsize;
		        if( pos.x < flatCamera.x - boardsize * 0.5 ) pos.x += boardsize;
		        if( pos.y > flatCamera.y + boardsize * 0.5 ) pos.y -= boardsize;
		        if( pos.y < flatCamera.y - boardsize * 0.5 ) pos.y += boardsize;
			}

	        let renderPos = new THREE.Vector3( pos.x, 0, -pos.y ); 
	    	
	    	if( this.radius > 0.75 )
	    	{
	    		debugLines.addCircle( renderPos, this.radius, new THREE.Vector3(0,1,0), this.color, 4 );
	    	}
	    	else
	    	{
		      	let top = renderPos.clone().add( new THREE.Vector3(0,0, this.radius) );
		      	let bottom = renderPos.clone().add( new THREE.Vector3(0,0,-this.radius) );
		      	let right = renderPos.clone().add( new THREE.Vector3(this.radius,0,0) );
		      	let left = renderPos.clone().add( new THREE.Vector3(-this.radius,0,0) );

		      	debugLines.addLine( top, bottom, this.color );
		      	debugLines.addLine( left, right, this.color );
	    	}
	      	


		}
	}

	shift( x, y ) {
		this.pos.x += x;
		this.pos.y += y;
	}

}