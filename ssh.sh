#!/bin/bash

SERVER_NAME=Murmuration
KEY_PATH=~/.aws/private-keys/mjh-key-pair-uswest2.pem

PUBLIC_IP=`./get-ip.sh $SERVER_NAME` || exit 1

# SSH in!
ssh -i ${KEY_PATH} ubuntu@${PUBLIC_IP}
